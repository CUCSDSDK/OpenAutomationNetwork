package com.cloupia.feature.network.lovs;

import java.util.List;

import com.cloupia.feature.network.accounts.connector.GetAllServers;
import com.cloupia.feature.network.accounts.inventory.model.Server;
import com.cloupia.model.cIM.FormLOVPair;
import com.cloupia.service.cIM.inframgr.forms.wizard.LOVProviderIf;
import com.cloupia.service.cIM.inframgr.forms.wizard.WizardSession;

public class ServerNameProvider implements LOVProviderIf {

	public static final String SERVER_NAME_LOV = "serverNameLOV";

	@Override
	public FormLOVPair[] getLOVs(WizardSession session) {
		FormLOVPair[] pairs = null;
		List<Server> sl = GetAllServers.getAllSlbServers("http",
				"192.168.212.153", "admin", "a10", 80);
		if (!sl.isEmpty()) {
			pairs = new FormLOVPair[sl.size()];
			for (int i = 0; i < sl.size(); i++) {
				Server s = sl.get(i);
				pairs[i] = new FormLOVPair(s.getName(), s.getName());
			}
		}
		return pairs;
	}
}
