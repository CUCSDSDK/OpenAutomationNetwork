package com.cloupia.feature.network.accounts.util;

import org.apache.log4j.Logger;

import com.cisco.cuic.api.client.JSON;
import com.cloupia.feature.network.accounts.NetworkAccount;
import com.cloupia.lib.connector.account.AbstractInfraAccount;
import com.cloupia.lib.connector.account.AccountUtil;
import com.cloupia.lib.connector.account.PhysicalInfraAccount;

public class NetworkAccountPersistenceUtil {

	static Logger logger = Logger.getLogger(NetworkAccountPersistenceUtil.class);

	public static void persistCollectedInventory(String accountName)
			throws Exception {
		logger.debug("Call in persistCollectedInventory :: inventory  ");
		logger.debug("Account Name " + accountName);

		NetworkAccount acc = getA10Credential(accountName);

		if (acc != null) {
			logger.debug("Remote Host Ip " + acc.getServerAddress());

		}
	}

	/**
	 * To get the object of Foo by passing the AccountName.
	 * 
	 * @param accountName
	 * @return
	 * @throws Exception
	 */
	public static NetworkAccount getA10Credential(String accountName)
			throws Exception {
		PhysicalInfraAccount acc = AccountUtil.getAccountByName(accountName);
		String json = acc.getCredential();
		AbstractInfraAccount specificAcc = (AbstractInfraAccount) JSON
				.jsonToJavaObject(json, NetworkAccount.class);
		specificAcc.setAccount(acc);

		return (NetworkAccount) specificAcc;

	}

}
