package com.cloupia.feature.network.accounts;

import org.apache.log4j.Logger;

import com.cisco.cuic.api.client.JSON;
import com.cloupia.feature.network.accounts.util.NetworkAccountPersistenceUtil;
import com.cloupia.lib.connector.InventoryContext;
import com.cloupia.lib.connector.InventoryEventListener;
import com.cloupia.lib.connector.account.AbstractInfraAccount;
import com.cloupia.lib.connector.account.AccountTypeEntry;
import com.cloupia.lib.connector.account.AccountUtil;
import com.cloupia.lib.connector.account.PhysicalAccountManager;
import com.cloupia.lib.connector.account.PhysicalConnectivityStatus;
import com.cloupia.lib.connector.account.PhysicalInfraAccount;

public class NetworkInventoryListener implements InventoryEventListener {
	private static Logger logger = Logger.getLogger(NetworkInventoryListener.class);

	@Override
	public void afterInventoryDone(String accountName, InventoryContext context)
			throws Exception {
		// TODO Auto-generated method stub
		logger.info("Call in NetworkInventoryListener afterInventoryDone");
		
		NetworkAccountPersistenceUtil.persistCollectedInventory(accountName);
		
		AccountTypeEntry entry = PhysicalAccountManager.getInstance().getAccountTypeEntryByName(accountName);
		PhysicalConnectivityStatus connectivityStatus =null;
		if(entry != null)
		{
			connectivityStatus = entry.getTestConnectionHandler().testConnection(accountName);
		}
		
		NetworkAccount acc = getA10Credential(accountName);
		
		if(acc != null && connectivityStatus != null)
		{
			logger.info("Inventory collected successfully");
		}

	}

	@Override
	public void beforeInventoryStart(String accountName, InventoryContext arg1)
			throws Exception {
		logger.info("Call in NetworkInventoryListener beforeInventoryStart");
		
	}
	
	private static NetworkAccount getA10Credential(String accountName) throws Exception{
		PhysicalInfraAccount acc = AccountUtil.getAccountByName(accountName);
		String json = acc.getCredential();
		AbstractInfraAccount specificAcc  =  (AbstractInfraAccount) JSON.jsonToJavaObject(json, NetworkAccount.class);
		specificAcc.setAccount(acc);
		
		return (NetworkAccount) specificAcc;
		
	}

}
