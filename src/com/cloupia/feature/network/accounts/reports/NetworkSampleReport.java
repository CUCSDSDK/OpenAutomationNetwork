package com.cloupia.feature.network.accounts.reports;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.constants.ModuleConstants;
import com.cloupia.model.cIM.DynReportContext;
import com.cloupia.model.cIM.ReportContextRegistry;
import com.cloupia.model.cIM.UIMenuLHTreeNode;
import com.cloupia.service.cIM.inframgr.reportengine.ContextMapRule;
import com.cloupia.service.cIM.inframgr.reports.simplified.CloupiaReportAction;
import com.cloupia.service.cIM.inframgr.reports.simplified.CloupiaReportWithActions;

public class NetworkSampleReport extends CloupiaReportWithActions {
	Logger logger = Logger.getLogger(NetworkSampleReport.class);
	
	private static final String NAME = "A10.account.sample.report";
	private static final String LABEL = "A10 Account Sample";

	public NetworkSampleReport() {
		super();
		//IMPORTANT: this tells the framework which column of this report you want to pass as the report context id
		//when there is a UI action being launched in this report
		this.setMgmtColumnIndex(0);
		
		//you can technically speaking set the proper menu id and context map rules here in the
		//the constructor BUT i think it's better to override the functions and hardcode this logic into the function 
		//instead (as shown below), just to guarantee it can't be tweaked by anyone else!
		
		
	}

	@Override
	public CloupiaReportAction[] getActions() {
		return null;
	}

	@Override
	public Class getImplementationClass() {
		//this class handles all the report generation logic, look here for more details
		return NetworkSampleReportImpl.class;
	}

	@Override
	public String getReportLabel() {
		return LABEL;
	}

	@Override
	public String getReportName() {
		return NAME;
	}

	//this is important, make sure this returns false.  isEasyReport should ONLY return
	//true if and only if you're using the POJO approach, which we aren't in this case.
	@Override
	public boolean isEasyReport() {
		return false;
	}

	//make sure to return true in this case, as there are no drill down reports
	@Override
	public boolean isLeafReport() {
		return false;
	}
	
	//basically hardcoding it so this report will show in the location i want it to
	//Physcial -> Storage -> LH Menu Tree Provider is 51.
	//Physcial -> Compute -> LH Menu Tree Provider is 50.
	//Physcial -> Network -> LH Menu Tree Provider is 52
	@Override
	public int getMenuID() {
	logger.info("getMenuID Method Called successfully");
		return 52;
	}
	
	@Override
	public ContextMapRule[] getMapRules() {
		logger.info("getMaprule Method Called successfully");
		DynReportContext dummyContextOneType = ReportContextRegistry.getInstance().getContextByName(ModuleConstants.INFRA_ACCOUNT_TYPE);
		ContextMapRule rule = new ContextMapRule();
		rule.setContextName(dummyContextOneType.getId());
		rule.setContextType(dummyContextOneType.getType());
		
		
		ContextMapRule[] rules = new ContextMapRule[1];
		rules[0] = rule;
		return rules;
	}

	

}
