package com.cloupia.feature.network.accounts.reports;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * This is the one pojo class 
 * Here we are declaring all the details for the report which are showing in UI
 *  
 * 
 */
@PersistenceCapable(detachable = "true", table = "A10_report_data")
public class NetworkSampleReportData {

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long id;
	
	@Persistent
	private String vCPUs;
	@Persistent
	private String disk;
	@Persistent
	private String ram;
	@Persistent
	private String vCPUHours;
	
	@Persistent
	private String diskGBHours;
	
	
	/**
	 * 
	 */
	public NetworkSampleReportData() {
		
	}

	

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}



	/**
	 * @return the vCPUs
	 */
	public String getvCPUs() {
		return vCPUs;
	}



	/**
	 * @param vCPUs the vCPUs to set
	 */
	public void setvCPUs(String vCPUs) {
		this.vCPUs = vCPUs;
	}



	/**
	 * @return the disk
	 */
	public String getDisk() {
		return disk;
	}



	/**
	 * @param disk the disk to set
	 */
	public void setDisk(String disk) {
		this.disk = disk;
	}



	/**
	 * @return the ram
	 */
	public String getRam() {
		return ram;
	}



	/**
	 * @param ram the ram to set
	 */
	public void setRam(String ram) {
		this.ram = ram;
	}



	/**
	 * @return the vCPUHours
	 */
	public String getvCPUHours() {
		return vCPUHours;
	}



	/**
	 * @param vCPUHours the vCPUHours to set
	 */
	public void setvCPUHours(String vCPUHours) {
		this.vCPUHours = vCPUHours;
	}



	/**
	 * @return the diskGBHours
	 */
	public String getDiskGBHours() {
		return diskGBHours;
	}



	/**
	 * @param diskGBHours the diskGBHours to set
	 */
	public void setDiskGBHours(String diskGBHours) {
		this.diskGBHours = diskGBHours;
	}

	

}
