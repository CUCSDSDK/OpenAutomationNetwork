package com.cloupia.feature.network.accounts.reports;

import java.util.List;


import org.apache.log4j.Logger;

import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.model.cIM.ReportContext;
import com.cloupia.model.cIM.TabularReport;
import com.cloupia.service.cIM.inframgr.TabularReportGeneratorIf;
import com.cloupia.service.cIM.inframgr.reportengine.ReportRegistryEntry;
import com.cloupia.service.cIM.inframgr.reports.TabularReportInternalModel;

/**
 * This is ReportImplementation class  used for setting context and defining columns and headers 
 * And showing the column data collecting from POJO class(A10Samplereportdata.java)
 * 
 * 
 */

public class NetworkSampleReportImpl implements TabularReportGeneratorIf {

		private static Logger logger = Logger
				.getLogger(NetworkSampleReportImpl.class);

		@Override
		public TabularReport getTabularReportReport(
				ReportRegistryEntry reportEntry, ReportContext context)
				throws Exception {
			
			logger.info("getTabularReport Method Called successfully");

			TabularReport report = new TabularReport();

			report.setGeneratedTime(System.currentTimeMillis());
			report.setReportName(reportEntry.getReportLabel());
			report.setContext(context);
			/**
			 * This is dummy account retrieval from database. Data populating from
			 * A10AccountData for this account.
			 */
			
			
			ObjStore<NetworkSampleReportData> a10SampleReportData = ObjStoreHelper
					.getStore(NetworkSampleReportData.class);
			List<NetworkSampleReportData> objs = a10SampleReportData.queryAll();

			TabularReportInternalModel model = new TabularReportInternalModel();
			/**
			 * Here we are adding  the column name 
			 *  
			 * 
			 */
			model.addTextColumn("VCPUs", "VCPUs");
			model.addTextColumn("Disk", "Disk");
			model.addTextColumn("RAM", "RAM");
			model.addTextColumn("VCPU Hours","VCPU Hours");
			model.addTextColumn("Disk GB Hours","Disk GB Hours");
			model.completedHeader();

			for (int i = 0; i < objs.size(); i++) {
				NetworkSampleReportData pojo = objs.get(i);
				model.addTextValue(pojo.getvCPUs());
				model.addTextValue(pojo.getDisk());
				model.addTextValue(pojo.getRam());
				model.addTextValue(pojo.getvCPUHours());
				model.addTextValue(pojo.getDiskGBHours());
				model.completedRow();
			}

			model.updateReport(report);

			return report;
		}

}
