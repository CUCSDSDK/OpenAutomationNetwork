package com.cloupia.feature.network.accounts.inventory;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.inventory.model.Server;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.service.cIM.inframgr.collector.controller.PersistenceListener;
import com.cloupia.service.cIM.inframgr.collector.model.ItemResponse;

/**
 * This is listener for persisting the slb server objects. 
 * 
 */
public class ServerListener extends
		PersistenceListener {
	static Logger logger = Logger.getLogger(ServerListener.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public void persistItem(ItemResponse res) throws Exception {
		
		ObjStore<Server> store = ObjStoreHelper.getStore(Server.class);
		List<Server> sl = (List<Server>)res.getBoundObjects();
		if (!sl.isEmpty()) {
			   Server s = sl.get(0);
			   String query = "accountName == '" + s.getAccountName() + "'";
			   store.delete(query);
		}
		for (Server s: sl)
			  logger.info("ServerListener -" + s.toString());
		store.insert((List<Server>)res.getBoundObjects());
			
	}

}
