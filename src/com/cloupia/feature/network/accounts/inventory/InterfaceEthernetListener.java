package com.cloupia.feature.network.accounts.inventory;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.inventory.model.Ethernet;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.service.cIM.inframgr.collector.controller.PersistenceListener;
import com.cloupia.service.cIM.inframgr.collector.model.ItemResponse;
/**
 * This is listener for persisting the inventory. 
 * 
 */
public class InterfaceEthernetListener extends
		PersistenceListener {
	static Logger logger = Logger.getLogger(InterfaceEthernetListener.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public void persistItem(ItemResponse res) throws Exception {
	
		ObjStore<Ethernet> store = ObjStoreHelper.getStore(Ethernet.class);
		
		List<Ethernet> el = (List<Ethernet>)res.getBoundObjects();
		if (!el.isEmpty()) {
		   Ethernet e = el.get(0);
		   String query = "accountName == '" + e.getAccountName() + "'";
		   store.delete(query);
		}
		
		for (Ethernet e: el)
			  logger.info("A10CollectorInventoryPersistenceListener -" + e.toString());
		store.insert((List<Ethernet>)res.getBoundObjects());
			
	}

}
