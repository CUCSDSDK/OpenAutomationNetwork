package com.cloupia.feature.network.accounts.inventory;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cisco.cuic.api.client.JSON;
import com.cloupia.feature.network.accounts.NetworkAccount;
import com.cloupia.feature.network.accounts.api.NetworkAccountJSONBinder;
import com.cloupia.feature.network.accounts.connector.GetAllServers;
import com.cloupia.feature.network.accounts.inventory.model.Server;
import com.cloupia.lib.connector.AbstractInventoryItemHandler;
import com.cloupia.lib.connector.InventoryContext;
import com.cloupia.lib.connector.account.AbstractInfraAccount;
import com.cloupia.lib.connector.account.AccountUtil;
import com.cloupia.lib.connector.account.PhysicalInfraAccount;
import com.cloupia.service.cIM.inframgr.collector.controller.PersistenceListener;
import com.cloupia.service.cIM.inframgr.collector.model.ItemResponse;

public class ServerItemHandler extends AbstractInventoryItemHandler {

	private static Logger logger = Logger.getLogger(ServerItemHandler.class);
	
	/**
	 * This method used for cleanup Item Inventory.
	 * method of InventoryItemHandlerIf interface
	 * @param accountName
	 * @return void
	 * @exception Exception
	 */
	@Override
	public void cleanup(String accountName) throws Exception {
		// TODO Auto-generated method stub

	}
	/** 
	 * This method used for do Inventory of Account
	 * @Override method of IInventoryItemHandlerIf interface
	 * @param accountName ,InventoryContext
	 * @return void
	 * @exception Exception
	 */
	@Override
	public void doInventory(String accountName, InventoryContext inventoryCtxt)
			throws Exception {
		doInventory(accountName);

	}
	/**
	 * This method used for do Inventory of Account
	 * @Override method of IInventoryItemHandlerIf interface
	 * @param accountName ,Object
	 * @return void
	 * @exception Exception
	 */
	@Override
	public void doInventory(String accountName, Object obj) throws Exception {
		// TODO Auto-generated method stub

	}
	
	/** private Method used for doing Inventory of Account
	 * @param accountName
	 * @return void
	 * @exception Exception
	 */
	private void doInventory(String accountName) throws Exception {
		
		NetworkAccount acc = getA10Credential(accountName);
		
		logger.info("ServerItemHandler:doInventory-" +acc.getLogin());
		logger.info("ServerItemHandler:doInventory-" +acc.getPassword());
		logger.info("ServerItemHandler:doInventory-" +acc.getDeviceIp());
		
		List<Server> sl = GetAllServers.getAllSlbServers(acc.getProtocol(),
				acc.getDeviceIp(), acc.getLogin(), acc.getPassword(),Integer.parseInt(acc.getPort()));
		
		for (Server s: sl) {
			  s.setAccountName(accountName);
			  logger.info("ServerItemHandler:doInventory-" + s.toString());
		}
		
		ItemResponse bindedResponse = new ItemResponse();
		bindedResponse.setBoundObjects(sl);
	
		PersistenceListener listener = getListener();
		if(listener != null)
		{
			logger.info("Calling for Persistence");
			listener.persistItem(bindedResponse);
		}
		else
		{
			logger.info("Persistence is null");
		}
	
	}

	/** Method used for get Url
	 * @param No
	 * @return String
	 * @exception No
	 */
	public String getUrl() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/** Method used to get object of  FooAccountJSONBinder
	 * 	Binder will bind the respective object as JSON. 
	 * @param No
	 * @return FooAccountJSONBinder
	 * @exception No
	 */
	public NetworkAccountJSONBinder getBinder() {
		// TODO Auto-generated method stub
		return new NetworkAccountJSONBinder();
	}
	
	/** Private Method used to get Map of Context
	 * @param accountName
	 * @return Map<String, Object>
	 * @exception No
	 */
	private Map<String, Object> getContext(String accountName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/** Private Method used to get Object of PersistenceListener
	 * @param No
	 * @return PersistenceListener
	 * @exception No
	 */
	private PersistenceListener getListener() {
		// TODO Auto-generated method stub
		return new ServerListener();
	}
	/** Private Method used to get Credential of account Name
	 * @param accountName
	 * @return FooAccount
	 * @exception Exception
	 */
	private static NetworkAccount getA10Credential(String accountName) throws Exception{
		PhysicalInfraAccount acc = AccountUtil.getAccountByName(accountName);
		String json = acc.getCredential();
		AbstractInfraAccount specificAcc  =  (AbstractInfraAccount) JSON.jsonToJavaObject(json, NetworkAccount.class);
		specificAcc.setAccount(acc);
		
		return (NetworkAccount) specificAcc;
	}

}
