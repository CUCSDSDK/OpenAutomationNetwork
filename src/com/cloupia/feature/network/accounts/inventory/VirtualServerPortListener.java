package com.cloupia.feature.network.accounts.inventory;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.inventory.model.VirtualServerPort;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.service.cIM.inframgr.collector.controller.PersistenceListener;
import com.cloupia.service.cIM.inframgr.collector.model.ItemResponse;

/**
 * This is listener for persisting the slb server port objects. 
 * 
 */
public class VirtualServerPortListener extends
		PersistenceListener {
	static Logger logger = Logger.getLogger(VirtualServerPortListener.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public void persistItem(ItemResponse res) throws Exception {
		
		ObjStore<VirtualServerPort> store = ObjStoreHelper.getStore(VirtualServerPort.class);
		// store.deleteAll();
		List<VirtualServerPort> vspl = (List<VirtualServerPort>)res.getBoundObjects();
		if (!vspl.isEmpty()) {
			   VirtualServerPort vsp = vspl.get(0);
			   String query = "accountName == '" + vsp.getAccountName() + "'";
			   store.delete(query);
		}
		for (VirtualServerPort vsp: vspl)
			  logger.info("VirtualServerPortListener -" + vsp.toString());
		store.insert((List<VirtualServerPort>)res.getBoundObjects());
			
	}

}
