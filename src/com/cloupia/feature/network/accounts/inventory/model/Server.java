package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_server_detail")
public class Server implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;
	
	@Persistent
	String device;

	@Persistent
	String name;
	
	@Persistent
	String host;
	
	@Persistent
	String action;
	
	@Persistent
	@SerializedName("template-server")
	String templateServer;
	
	@Persistent
	@SerializedName("health-check-disable")
	Integer healthCheckDisable;
	
	@Persistent
	@SerializedName("conn-limit")
	Integer connLimit;
	
	@Persistent
	@SerializedName("no-logging")
	Integer noLogging;
	
	@Persistent
	Integer weight;
	
	@Persistent
	@SerializedName("slow-start")
	Integer slowStart;
	
	
	@Persistent
	@SerializedName("spoofing-cache")
	Integer spoofingCache;
	
	@Persistent
	@SerializedName("stats-data-action")
	String statsDataAction;
	
	@Persistent
	@SerializedName("extended-stats")
	Integer extendedStats;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	
	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getTemplateServer() {
		return templateServer;
	}

	public void setTemplateServer(String templateServer) {
		this.templateServer = templateServer;
	}

	public Integer getHealthCheckDisable() {
		return healthCheckDisable;
	}

	public void setHealthCheckDisable(Integer healthCheckDisable) {
		this.healthCheckDisable = healthCheckDisable;
	}

	public Integer getConnLimit() {
		return connLimit;
	}

	public void setConnLimit(Integer connLimit) {
		this.connLimit = connLimit;
	}

	public Integer getNoLogging() {
		return noLogging;
	}

	public void setNoLogging(Integer noLogging) {
		this.noLogging = noLogging;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getSlowStart() {
		return slowStart;
	}

	public void setSlowStart(Integer slowStart) {
		this.slowStart = slowStart;
	}

	public Integer getSpoofingCache() {
		return spoofingCache;
	}

	public void setSpoofingCache(Integer spoofingCache) {
		this.spoofingCache = spoofingCache;
	}

	public String getStatsDataAction() {
		return statsDataAction;
	}

	public void setStatsDataAction(String statsDataAction) {
		this.statsDataAction = statsDataAction;
	}

	public Integer getExtendedStats() {
		return extendedStats;
	}

	public void setExtendedStats(Integer extendedStats) {
		this.extendedStats = extendedStats;
	}

	@Override
	public String toString() {
		return "Server [accountName=" + accountName + ", device=" + device
				+ ", name=" + name + ", device=" + host + ", action=" + action
				+ ", templateServer=" + templateServer
				+ ", healthCheckDisable=" + healthCheckDisable + ", connLimit="
				+ connLimit + ", noLogging=" + noLogging + ", weight=" + weight
				+ ", slowStart=" + slowStart + ", spoofingCache="
				+ spoofingCache + ", statsDataAction=" + statsDataAction
				+ ", extendedStats=" + extendedStats + "]";
	}
	
	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}

}
