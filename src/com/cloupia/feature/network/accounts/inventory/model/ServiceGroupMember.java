package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_service_group_member_detail")
public class ServiceGroupMember implements InventoryDBItemIf, ReportableIf {
	
	@Persistent
	private String accountName;
	
	@Persistent
	String device;
	
	@Persistent
	String serviceGroupName;
	
	@Persistent
	String name;
	
	@Persistent
	Integer port;

	@Persistent
	@SerializedName("member-state")
	String memberState;

	@Persistent
	@SerializedName("member-stats-data-disable")
	Integer memberStatsDataDisable;

	@Persistent
	@SerializedName("member-priority")
	Integer memberPriority;
	
	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getServiceGroupName() {
		return serviceGroupName;
	}

	public void setServiceGroupName(String serviceGroupName) {
		this.serviceGroupName = serviceGroupName;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getMemberState() {
		return memberState;
	}

	public void setMemberState(String memberState) {
		this.memberState = memberState;
	}

	public Integer getMemberStatsDataDisable() {
		return memberStatsDataDisable;
	}

	public void setMemberStatsDataDisable(Integer memberStatsDataDisable) {
		this.memberStatsDataDisable = memberStatsDataDisable;
	}

	public Integer getMemberPriority() {
		return memberPriority;
	}

	public void setMemberPriority(Integer memberPriority) {
		this.memberPriority = memberPriority;
	}

	@Override
	public String toString() {
		return "ServiceGroupMember [device=" + device + ", serviceGroupName="
				+ serviceGroupName + ", name=" + name + ", port=" + port
				+ ", memberState=" + memberState + ", memberStatsDataDisable="
				+ memberStatsDataDisable + ", memberPriority=" + memberPriority
				+ "]";
	}
	
	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}
}
