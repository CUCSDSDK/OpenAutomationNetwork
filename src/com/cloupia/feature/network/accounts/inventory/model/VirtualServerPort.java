package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_virtual_server_port_detail")
public class VirtualServerPort implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;
	
	@Persistent
	String device;
	
	@Persistent
	String virtualServerName;
	
	@Persistent
	@SerializedName("port-number") 
	Integer portNumber;
	
	@Persistent
	String protocol;
	
	@Persistent
	Integer range;
	
	@Persistent
	@SerializedName("conn-limit")
	Integer connLimit;
	
	@Persistent
	Integer reset;
	
	@Persistent
	@SerializedName("no-logging")
	Integer noLogging;
		
	@Persistent
	String action;
	
	@Persistent
	@SerializedName("def-selection-if-pref-failed")
	String defSelectionIfPrefFailed;
	
	@Persistent
	@SerializedName("skip-rev-hash")
	Integer skipRevHash;
	
	@Persistent
	@SerializedName("message-switching")
	Integer messageSwitching;
	
	@Persistent
	@SerializedName("force-routing-mode")
	Integer forceRoutingMode;
	
	@Persistent
	@SerializedName("reset-on-server-selection-fail")
	Integer resetOnServerSelectionFail;
	
	@Persistent
	@SerializedName("clientip-sticky-nat")
	Integer clientipStickyNat;
	
	@Persistent
	@SerializedName("extended-stats")
	Integer extendedStats;
	
	@Persistent
	@SerializedName("snat-on-vip")
	Integer snatOnVip;
	
	@Persistent
	@SerializedName("stats-data-action")
	String statsDataAction;
	
	@Persistent
	@SerializedName("syn-cookie")
	Integer synCookie;
	
	@Persistent
	@SerializedName("no-auto-up-on-aflex")
	Integer noAutoUpOnAflex;
	
	@Persistent
	@SerializedName("no-dest-nat")
	Integer noDestNat;
		
	@Persistent
	@SerializedName("scaleout-bucket-count")
	Integer scaleoutBucketCount;
	
	@Persistent
	Integer auto;
	
	@Persistent
	@SerializedName("service-group")
	String serviceGroup;
	
	@Persistent
	Integer ipinip;
	
	@Persistent
	@SerializedName("rtp-sip-call-id-match")
	Integer rtpSipCallIdMatch;
	
	@Persistent
	@SerializedName("use-rcv-hop-for-resp")
	Integer useRcvHopForResp;
	
	@Persistent
	@SerializedName("template-client-ssl")
	String templateClientSsl;
	
	@Persistent
	@SerializedName("template-virtual-port")
	String templateVirtualPort;
	
	@Persistent
	@SerializedName("use-default-if-no-server")
	Integer useDefaultIfNoServer;

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}
	
	public String getVirtualServerName() {
		return virtualServerName;
	}

	public void setVirtualServerName(String virtualServerName) {
		this.virtualServerName = virtualServerName;
	}
	
	public Integer getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(Integer portNumber) {
		this.portNumber = portNumber;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public Integer getRange() {
		return range;
	}

	public void setRange(Integer range) {
		this.range = range;
	}

	public Integer getConnLimit() {
		return connLimit;
	}

	public void setConnLimit(Integer connLimit) {
		this.connLimit = connLimit;
	}

	public Integer getReset() {
		return reset;
	}

	public void setReset(Integer reset) {
		this.reset = reset;
	}

	public Integer getNoLogging() {
		return noLogging;
	}

	public void setNoLogging(Integer noLogging) {
		this.noLogging = noLogging;
	}

	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	
	public String getDefSelectionIfPrefFailed() {
		return defSelectionIfPrefFailed;
	}

	public void setDefSelectionIfPrefFailed(String defSelectionIfPrefFailed) {
		this.defSelectionIfPrefFailed = defSelectionIfPrefFailed;
	}

	public Integer getSkipRevHash() {
		return skipRevHash;
	}

	public void setSkipRevHash(Integer skipRevHash) {
		this.skipRevHash = skipRevHash;
	}

	public Integer getMessageSwitching() {
		return messageSwitching;
	}

	public void setMessageSwitching(Integer messageSwitching) {
		this.messageSwitching = messageSwitching;
	}

	public Integer getForceRoutingMode() {
		return forceRoutingMode;
	}

	public void setForceRoutingMode(Integer forceRoutingMode) {
		this.forceRoutingMode = forceRoutingMode;
	}

	public Integer getResetOnServerSelectionFail() {
		return resetOnServerSelectionFail;
	}

	public void setResetOnServerSelectionFail(Integer resetOnServerSelectionFail) {
		this.resetOnServerSelectionFail = resetOnServerSelectionFail;
	}

	public Integer getClientipStickyNat() {
		return clientipStickyNat;
	}

	public void setClientipStickyNat(Integer clientipStickyNat) {
		this.clientipStickyNat = clientipStickyNat;
	}

	public Integer getExtendedStats() {
		return extendedStats;
	}

	public void setExtendedStats(Integer extendedStats) {
		this.extendedStats = extendedStats;
	}

	public Integer getSnatOnVip() {
		return snatOnVip;
	}

	public void setSnatOnVip(Integer snatOnVip) {
		this.snatOnVip = snatOnVip;
	}

	public String getStatsDataAction() {
		return statsDataAction;
	}

	public void setStatsDataAction(String statsDataAction) {
		this.statsDataAction = statsDataAction;
	}

	public Integer getSynCookie() {
		return synCookie;
	}

	public void setSynCookie(Integer synCookie) {
		this.synCookie = synCookie;
	}

	public Integer getNoAutoUpOnAflex() {
		return noAutoUpOnAflex;
	}

	public void setNoAutoUpOnAflex(Integer noAutoUpOnAflex) {
		this.noAutoUpOnAflex = noAutoUpOnAflex;
	}

	public Integer getNoDestNat() {
		return noDestNat;
	}

	public void setNoDestNat(Integer noDestNat) {
		this.noDestNat = noDestNat;
	}

	
	public Integer getScaleoutBucketCount() {
		return scaleoutBucketCount;
	}

	public void setScaleoutBucketCount(Integer scaleoutBucketCount) {
		this.scaleoutBucketCount = scaleoutBucketCount;
	}

	public Integer getAuto() {
		return auto;
	}

	public void setAuto(Integer auto) {
		this.auto = auto;
	}

	public Integer getIpinip() {
		return ipinip;
	}

	public void setIpinip(Integer ipinip) {
		this.ipinip = ipinip;
	}

	public Integer getRtpSipCallIdMatch() {
		return rtpSipCallIdMatch;
	}

	public void setRtpSipCallIdMatch(Integer rtpSipCallIdMatch) {
		this.rtpSipCallIdMatch = rtpSipCallIdMatch;
	}

	public Integer getUseRcvHopForResp() {
		return useRcvHopForResp;
	}

	public void setUseRcvHopForResp(Integer useRcvHopForResp) {
		this.useRcvHopForResp = useRcvHopForResp;
	}

	public String getTemplateVirtualPort() {
		return templateVirtualPort;
	}

	public void setTemplateVirtualPort(String templateVirtualPort) {
		this.templateVirtualPort = templateVirtualPort;
	}

	@Override
	public String toString() {
		return "VirtualServerPort [accountName=" + accountName + ", device="
				+ device + ", virtualServerName=" + virtualServerName
				+ ", portNumber=" + portNumber + ", protocol=" + protocol
				+ ", range=" + range + ", connLimit=" + connLimit + ", reset="
				+ reset + ", noLogging=" + noLogging + ", action=" + action
				+ ", defSelectionIfPrefFailed=" + defSelectionIfPrefFailed
				+ ", skipRevHash=" + skipRevHash + ", messageSwitching="
				+ messageSwitching + ", forceRoutingMode=" + forceRoutingMode
				+ ", resetOnServerSelectionFail=" + resetOnServerSelectionFail
				+ ", clientipStickyNat=" + clientipStickyNat
				+ ", extendedStats=" + extendedStats + ", snatOnVip="
				+ snatOnVip + ", statsDataAction=" + statsDataAction
				+ ", synCookie=" + synCookie + ", noAutoUpOnAflex="
				+ noAutoUpOnAflex + ", noDestNat=" + noDestNat
				+ ", scaleoutBucketCount=" + scaleoutBucketCount + ", auto="
				+ auto + ", serviceGroup=" + serviceGroup + ", ipinip="
				+ ipinip + ", rtpSipCallIdMatch=" + rtpSipCallIdMatch
				+ ", useRcvHopForResp=" + useRcvHopForResp
				+ ", templateClientSsl=" + templateClientSsl
				+ ", templateVirtualPort=" + templateVirtualPort
				+ ", useDefaultIfNoServer=" + useDefaultIfNoServer + "]";
	}
	
	public String getServiceGroup() {
		return serviceGroup;
	}

	public void setServiceGroup(String serviceGroup) {
		this.serviceGroup = serviceGroup;
	}

	public String getTemplateClientSsl() {
		return templateClientSsl;
	}

	public void setTemplateClientSsl(String templateClientSsl) {
		this.templateClientSsl = templateClientSsl;
	}

	public Integer getUseDefaultIfNoServer() {
		return useDefaultIfNoServer;
	}

	public void setUseDefaultIfNoServer(Integer useDefaultIfNoServer) {
		this.useDefaultIfNoServer = useDefaultIfNoServer;
	}

	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}
	
}
