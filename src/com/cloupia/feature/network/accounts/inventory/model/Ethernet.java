package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_interface_ethernet_detail")
public class Ethernet implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;
	
	@Persistent
	String device;

	@Persistent
	Integer ifnum;

	@Persistent
	@SerializedName("l3-vlan-fwd-disable")
	Integer l3VlanFwdDisable;

	@Persistent
	Integer mtu;
	
	@Persistent
	@SerializedName("load-interval")
	Integer loadInterval;

	
	@SerializedName("trap-source")
	@Persistent
	Integer trapSource;

	@Persistent
	String duplexity;

	@Persistent
	String speed;

	@Persistent
	@SerializedName("flow-control")
	Integer flowControl;

	@Persistent
	String action;

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public Integer getIfnum() {
		return ifnum;
	}

	public void setIfnum(Integer ifnum) {
		this.ifnum = ifnum;
	}

	public Integer getL3VlanFwdDisable() {
		return l3VlanFwdDisable;
	}

	public void setL3VlanFwdDisable(Integer l3VlanFwdDisable) {
		this.l3VlanFwdDisable = l3VlanFwdDisable;
	}

	public Integer getLoadInterval() {
		return loadInterval;
	}

	public void setLoadInterval(Integer loadInterval) {
		this.loadInterval = loadInterval;
	}

	public Integer getMtu() {
		return mtu;
	}

	public void setMtu(Integer mtu) {
		this.mtu = mtu;
	}

	public Integer getTrapSource() {
		return trapSource;
	}

	public void setTrapSource(Integer trapSource) {
		this.trapSource = trapSource;
	}

	public String getDuplexity() {
		return duplexity;
	}

	public void setDuplexity(String duplexity) {
		this.duplexity = duplexity;
	}

	public String getSpeed() {
		return speed;
	}

	public void setSpeed(String speed) {
		this.speed = speed;
	}

	public Integer getFlowControl() {
		return flowControl;
	}

	public void setFlowControl(Integer flowControl) {
		this.flowControl = flowControl;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public String toString() {
		return "Ethernet [accountName=" + accountName + ", device=" + device
				+ ", ifnum=" + ifnum + ", l3VlanFwdDisable=" + l3VlanFwdDisable
				+ ", mtu=" + mtu + ", loadInterval=" + loadInterval
				+ ", trapSource=" + trapSource + ", duplexity=" + duplexity
				+ ", speed=" + speed + ", flowControl=" + flowControl
				+ ", action=" + action + "]";
	}

	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}

}
