package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.lib.easyui.annotations.ReportField;
import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_server_port_stats")
public class PortStats implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;

	@Persistent
	String device;

	@Persistent
	String serverName;

	@Persistent
	@SerializedName("port-number")
	Integer portNumber;

	@Persistent
	String protocol;

	@Persistent
	@SerializedName("curr_conn")
	Integer currConn;

	@Persistent
	@SerializedName("curr_req")
	Integer currReq;

	@Persistent
	@SerializedName("total_req")
	Integer totalReq;

	@Persistent
	@SerializedName("total_req_succ")
	Integer totalReqSucc;

	@Persistent
	@SerializedName("total_fwd_bytes")
	Integer totalFwdBytes;

	@Persistent
	@SerializedName("total_fwd_pkts")
	Integer totalFwdPkts;

	@Persistent
	@SerializedName("total_rev_bytes")
	Integer totalRevBytes;

	@Persistent
	@SerializedName("total_rev_pkts")
	Integer totalRevPkts;

	@Persistent
	@SerializedName("total_conn")
	Integer totalConn;

	@Persistent
	@SerializedName("last_total_conn")
	Integer lastTotalConn;

	@Persistent
	@SerializedName("peak_conn")
	Integer peakConn;

	@Persistent
	@SerializedName("es_resp_200")
	Integer esResp200;

	@Persistent
	@SerializedName("es_resp_300")
	Integer esResp300;

	@Persistent
	@SerializedName("es_resp_400")
	Integer esResp400;

	@Persistent
	@SerializedName("es_resp_500")
	Integer esResp500;

	@Persistent
	@SerializedName("es_resp_other")
	Integer esRespOther;

	@Persistent
	@SerializedName("es_req_count")
	Integer esReqCount;

	@Persistent
	@SerializedName("es_resp_count")
	Integer esRespCount;

	@Persistent
	@SerializedName("es_resp_invalid_http")
	Integer esRespInvalidHttp;

	@Persistent
	@SerializedName("total_rev_pkts_inspected")
	Integer totalRevPktsInspected;

	@Persistent
	@SerializedName("total_rev_pkts_inspected_good_status_code")
	Integer totalRevPktsInspectedGoodStatusCode;

	@Persistent
	@SerializedName("response_time")
	Integer responseTime;

	@Persistent
	@SerializedName("fastest_rsp_time")
	Integer fastestRspTime;

	@Persistent
	@SerializedName("slowest_rsp_time")
	Integer slowestRspTime;

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public Integer getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(Integer portNumber) {
		this.portNumber = portNumber;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public Integer getCurrConn() {
		return currConn;
	}

	public void setCurrConn(Integer currConn) {
		this.currConn = currConn;
	}

	public Integer getCurrReq() {
		return currReq;
	}

	public void setCurrReq(Integer currReq) {
		this.currReq = currReq;
	}

	public Integer getTotalReq() {
		return totalReq;
	}

	public void setTotalReq(Integer totalReq) {
		this.totalReq = totalReq;
	}

	public Integer getTotalReqSucc() {
		return totalReqSucc;
	}

	public void setTotalReqSucc(Integer totalReqSucc) {
		this.totalReqSucc = totalReqSucc;
	}

	public Integer getTotalFwdBytes() {
		return totalFwdBytes;
	}

	public void setTotalFwdBytes(Integer totalFwdBytes) {
		this.totalFwdBytes = totalFwdBytes;
	}

	public Integer getTotalFwdPkts() {
		return totalFwdPkts;
	}

	public void setTotalFwdPkts(Integer totalFwdPkts) {
		this.totalFwdPkts = totalFwdPkts;
	}

	public Integer getTotalRevBytes() {
		return totalRevBytes;
	}

	public void setTotalRevBytes(Integer totalRevBytes) {
		this.totalRevBytes = totalRevBytes;
	}

	public Integer getTotalRevPkts() {
		return totalRevPkts;
	}

	public void setTotalRevPkts(Integer totalRevPkts) {
		this.totalRevPkts = totalRevPkts;
	}

	public Integer getTotalConn() {
		return totalConn;
	}

	public void setTotalConn(Integer totalConn) {
		this.totalConn = totalConn;
	}

	public Integer getLastTotalConn() {
		return lastTotalConn;
	}

	public void setLastTotalConn(Integer lastTotalConn) {
		this.lastTotalConn = lastTotalConn;
	}

	public Integer getPeakConn() {
		return peakConn;
	}

	public void setPeakConn(Integer peakConn) {
		this.peakConn = peakConn;
	}

	public Integer getEsResp200() {
		return esResp200;
	}

	public void setEsResp200(Integer esResp200) {
		this.esResp200 = esResp200;
	}

	public Integer getEsResp300() {
		return esResp300;
	}

	public void setEsResp300(Integer esResp300) {
		this.esResp300 = esResp300;
	}

	public Integer getEsResp400() {
		return esResp400;
	}

	public void setEsResp400(Integer esResp400) {
		this.esResp400 = esResp400;
	}

	public Integer getEsResp500() {
		return esResp500;
	}

	public void setEsResp500(Integer esResp500) {
		this.esResp500 = esResp500;
	}

	public Integer getEsRespOther() {
		return esRespOther;
	}

	public void setEsRespOther(Integer esRespOther) {
		this.esRespOther = esRespOther;
	}

	public Integer getEsReqCount() {
		return esReqCount;
	}

	public void setEsReqCount(Integer esReqCount) {
		this.esReqCount = esReqCount;
	}

	public Integer getEsRespCount() {
		return esRespCount;
	}

	public void setEsRespCount(Integer esRespCount) {
		this.esRespCount = esRespCount;
	}

	public Integer getEsRespInvalidHttp() {
		return esRespInvalidHttp;
	}

	public void setEsRespInvalidHttp(Integer esRespInvalidHttp) {
		this.esRespInvalidHttp = esRespInvalidHttp;
	}

	public Integer getTotalRevPktsInspected() {
		return totalRevPktsInspected;
	}

	public void setTotalRevPktsInspected(Integer totalRevPktsInspected) {
		this.totalRevPktsInspected = totalRevPktsInspected;
	}

	public Integer getTotalRevPktsInspectedGoodStatusCode() {
		return totalRevPktsInspectedGoodStatusCode;
	}

	public void setTotalRevPktsInspectedGoodStatusCode(
			Integer totalRevPktsInspectedGoodStatusCode) {
		this.totalRevPktsInspectedGoodStatusCode = totalRevPktsInspectedGoodStatusCode;
	}

	public Integer getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Integer responseTime) {
		this.responseTime = responseTime;
	}

	public Integer getFastestRspTime() {
		return fastestRspTime;
	}

	public void setFastestRspTime(Integer fastestRspTime) {
		this.fastestRspTime = fastestRspTime;
	}

	public Integer getSlowestRspTime() {
		return slowestRspTime;
	}

	public void setSlowestRspTime(Integer slowestRspTime) {
		this.slowestRspTime = slowestRspTime;
	}
	
	@Override
	public String toString() {
		return "PortStats [accountName=" + accountName + ", device=" + device
				+ ", serverName=" + serverName + ", portNumber=" + portNumber
				+ ", protocol=" + protocol + ", currConn=" + currConn
				+ ", currReq=" + currReq + ", totalReq=" + totalReq
				+ ", totalReqSucc=" + totalReqSucc + ", totalFwdBytes="
				+ totalFwdBytes + ", totalFwdPkts=" + totalFwdPkts
				+ ", totalRevBytes=" + totalRevBytes + ", totalRevPkts="
				+ totalRevPkts + ", totalConn=" + totalConn
				+ ", lastTotalConn=" + lastTotalConn + ", peakConn=" + peakConn
				+ ", esResp200=" + esResp200 + ", esResp300=" + esResp300
				+ ", esResp400=" + esResp400 + ", esResp500=" + esResp500
				+ ", esRespOther=" + esRespOther + ", esReqCount=" + esReqCount
				+ ", esRespCount=" + esRespCount + ", esRespInvalidHttp="
				+ esRespInvalidHttp + ", totalRevPktsInspected="
				+ totalRevPktsInspected
				+ ", totalRevPktsInspectedGoodStatusCode="
				+ totalRevPktsInspectedGoodStatusCode + ", responseTime="
				+ responseTime + ", fastestRspTime=" + fastestRspTime
				+ ", slowestRspTime=" + slowestRspTime + "]";
	}

	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}

}
