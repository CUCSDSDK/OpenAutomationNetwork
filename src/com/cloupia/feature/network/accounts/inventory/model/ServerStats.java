package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.lib.easyui.annotations.ReportField;
import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_server_stats")
public class ServerStats implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;
	
	@ReportField(label = "Device")
	@Persistent
	String device;

	@Persistent
	String name;
	
	@Persistent
	@SerializedName("curr-conn")
	Integer currConn;
	
	@Persistent
	@SerializedName("total-conn")
	Integer totalConn;
	
	@Persistent
	@SerializedName("fwd-pkt")
	Integer fwdPkt;

	@Persistent
	@SerializedName("rev-pkt")
	Integer revPkt;
	
	@Persistent
	@SerializedName("peak-conn")
	Integer peakConn;
	
	@Override
	public String toString() {
		return "ServerStats [accountName=" + accountName + ", device=" + device
				+ ", name=" + name + ", currConn=" + currConn + ", totalConn="
				+ totalConn + ", fwdPkt=" + fwdPkt + ", revPkt=" + revPkt
				+ ", peakConn=" + peakConn + "]";
	}
	
	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCurrConn() {
		return currConn;
	}

	public void setCurrConn(Integer currConn) {
		this.currConn = currConn;
	}

	public Integer getTotalConn() {
		return totalConn;
	}

	public void setTotalConn(Integer totalConn) {
		this.totalConn = totalConn;
	}

	public Integer getFwdPkt() {
		return fwdPkt;
	}

	public void setFwdPkt(Integer fwdPkt) {
		this.fwdPkt = fwdPkt;
	}

	public Integer getRevPkt() {
		return revPkt;
	}

	public void setRevPkt(Integer revPkt) {
		this.revPkt = revPkt;
	}

	public Integer getPeakConn() {
		return peakConn;
	}

	public void setPeakConn(Integer peakConn) {
		this.peakConn = peakConn;
	}
	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}

}
