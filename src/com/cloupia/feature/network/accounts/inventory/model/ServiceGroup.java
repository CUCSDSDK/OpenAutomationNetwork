package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.lib.easyui.annotations.ReportField;
import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_service_group_detail")
public class ServiceGroup implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;

	@Persistent
	String device;

	@Persistent
	String name;
	
	@Persistent
	String protocol;

	@Persistent
	@SerializedName("lb-method")
	String lbMethod;

	@Persistent
	@SerializedName("stateless-auto-switch")
	Integer statelessAutoSwitch;

	@Persistent
	@SerializedName("reset-on-server-selection-fail")
	Integer resetOnServerSelectionFail;

	@Persistent
	@SerializedName("priority-affinity")
	Integer priorityAffinity;

	@Persistent
	@SerializedName("backup-server-event-log")
	Integer backupServerEventLog;

	@Persistent
	@SerializedName("stats-data-action")
	String statsDataAction;

	@Persistent
	@SerializedName("extended-stats")
	Integer extendedStats;

	@Persistent
	@SerializedName("traffic-replication-mirror")
	Integer trafficReplicationMirror;

	@Persistent
	@SerializedName("traffic-replication-mirror-da-repl")
	Integer trafficReplicationMirrorDaRepl;

	@Persistent
	@SerializedName("traffic-replication-mirror-ip-repl")
	Integer trafficReplicationMirrorIpRepl;

	@Persistent
	@SerializedName("traffic-replication-mirror-sa-da-repl")
	Integer trafficReplicationMirrorSaDaRepl;

	@Persistent
	@SerializedName("traffic-replication-mirror-sa-repl")
	Integer trafficReplicationMirrorSaRepl;

	@Persistent
	@SerializedName("health-check-disable")
	Integer healthCheckDisable;

	@Persistent
	@SerializedName("sample-rsp-time")
	Integer sampleRspTime;

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getLbMethod() {
		return lbMethod;
	}

	public void setLbMethod(String lbMethod) {
		this.lbMethod = lbMethod;
	}

	public Integer getStatelessAutoSwitch() {
		return statelessAutoSwitch;
	}

	public void setStatelessAutoSwitch(Integer statelessAutoSwitch) {
		this.statelessAutoSwitch = statelessAutoSwitch;
	}

	public Integer getResetOnServerSelectionFail() {
		return resetOnServerSelectionFail;
	}

	public void setResetOnServerSelectionFail(Integer resetOnServerSelectionFail) {
		this.resetOnServerSelectionFail = resetOnServerSelectionFail;
	}

	public Integer getPriorityAffinity() {
		return priorityAffinity;
	}

	public void setPriorityAffinity(Integer priorityAffinity) {
		this.priorityAffinity = priorityAffinity;
	}

	public Integer getBackupServerEventLog() {
		return backupServerEventLog;
	}

	public void setBackupServerEventLog(Integer backupServerEventLog) {
		this.backupServerEventLog = backupServerEventLog;
	}

	public String getStatsDataAction() {
		return statsDataAction;
	}

	public void setStatsDataAction(String statsDataAction) {
		this.statsDataAction = statsDataAction;
	}

	public Integer getExtendedStats() {
		return extendedStats;
	}

	public void setExtendedStats(Integer extendedStats) {
		this.extendedStats = extendedStats;
	}

	public Integer getTrafficReplicationMirror() {
		return trafficReplicationMirror;
	}

	public void setTrafficReplicationMirror(Integer trafficReplicationMirror) {
		this.trafficReplicationMirror = trafficReplicationMirror;
	}

	public Integer getTrafficReplicationMirrorDaRepl() {
		return trafficReplicationMirrorDaRepl;
	}

	public void setTrafficReplicationMirrorDaRepl(
			Integer trafficReplicationMirrorDaRepl) {
		this.trafficReplicationMirrorDaRepl = trafficReplicationMirrorDaRepl;
	}

	public Integer getTrafficReplicationMirrorIpRepl() {
		return trafficReplicationMirrorIpRepl;
	}

	public void setTrafficReplicationMirrorIpRepl(
			Integer trafficReplicationMirrorIpRepl) {
		this.trafficReplicationMirrorIpRepl = trafficReplicationMirrorIpRepl;
	}

	public Integer getTrafficReplicationMirrorSaDaRepl() {
		return trafficReplicationMirrorSaDaRepl;
	}

	public void setTrafficReplicationMirrorSaDaRepl(
			Integer trafficReplicationMirrorSaDaRepl) {
		this.trafficReplicationMirrorSaDaRepl = trafficReplicationMirrorSaDaRepl;
	}

	public Integer getTrafficReplicationMirrorSaRepl() {
		return trafficReplicationMirrorSaRepl;
	}

	public void setTrafficReplicationMirrorSaRepl(
			Integer trafficReplicationMirrorSaRepl) {
		this.trafficReplicationMirrorSaRepl = trafficReplicationMirrorSaRepl;
	}

	public Integer getHealthCheckDisable() {
		return healthCheckDisable;
	}

	public void setHealthCheckDisable(Integer healthCheckDisable) {
		this.healthCheckDisable = healthCheckDisable;
	}

	public Integer getSampleRspTime() {
		return sampleRspTime;
	}

	public void setSampleRspTime(Integer sampleRspTime) {
		this.sampleRspTime = sampleRspTime;
	}

	@Override
	public String toString() {
		return "ServiceGroup [device=" + device + ", name=" + name
				+ ", protocol=" + protocol + ", lbMethod=" + lbMethod
				+ ", statelessAutoSwitch=" + statelessAutoSwitch
				+ ", resetOnServerSelectionFail=" + resetOnServerSelectionFail
				+ ", priorityAffinity=" + priorityAffinity
				+ ", backupServerEventLog=" + backupServerEventLog
				+ ", statsDataAction=" + statsDataAction + ", extendedStats="
				+ extendedStats + ", trafficReplicationMirror="
				+ trafficReplicationMirror
				+ ", trafficReplicationMirrorDaRepl="
				+ trafficReplicationMirrorDaRepl
				+ ", trafficReplicationMirrorIpRepl="
				+ trafficReplicationMirrorIpRepl
				+ ", trafficReplicationMirrorSaDaRepl="
				+ trafficReplicationMirrorSaDaRepl
				+ ", trafficReplicationMirrorSaRepl="
				+ trafficReplicationMirrorSaRepl + ", healthCheckDisable="
				+ healthCheckDisable + ", sampleRspTime=" + sampleRspTime + "]";
	}

	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}

}
