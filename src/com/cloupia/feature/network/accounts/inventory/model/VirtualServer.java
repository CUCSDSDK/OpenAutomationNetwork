package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.lib.easyui.annotations.ReportField;
import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_virtual_server_detail")
public class VirtualServer implements InventoryDBItemIf, ReportableIf {
	
	@Persistent
	private String accountName;
	
	@ReportField(label = "Device")
	@Persistent
	String device;

	@Persistent
	String name;
	
	@Persistent
	@SerializedName("ip-address")
	String ipAddress;
	
	@Persistent
	@SerializedName("enable-disable-action")
	String enableDisableAction;
	
	@Persistent
	@SerializedName("redistribution-flagged")
	Integer redistributionFlagged;
	
	@Persistent
	@SerializedName("arp-disable")
	Integer arpDisable;
	
	@Persistent
	@SerializedName("stats-data-action")
	String statsDataAction;
	
	@Persistent
	@SerializedName("extended-stats")
	Integer extendedStats;
		

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getEnableDisableAction() {
		return enableDisableAction;
	}

	public void setEnableDisableAction(String enableDisableAction) {
		this.enableDisableAction = enableDisableAction;
	}

	public Integer getRedistributionFlagged() {
		return redistributionFlagged;
	}

	public void setRedistributionFlagged(Integer redistributionFlagged) {
		this.redistributionFlagged = redistributionFlagged;
	}

	public Integer getArpDisable() {
		return arpDisable;
	}

	public void setArpDisable(Integer arpDisable) {
		this.arpDisable = arpDisable;
	}
	
	public String getStatsDataAction() {
		return statsDataAction;
	}

	public void setStatsDataAction(String statsDataAction) {
		this.statsDataAction = statsDataAction;
	}

	public Integer getExtendedStats() {
		return extendedStats;
	}

	public void setExtendedStats(Integer extendedStats) {
		this.extendedStats = extendedStats;
	}
		
	
	@Override
	public String toString() {
		return "VirtualServer [accountName=" + accountName + ", device="
				+ device + ", name=" + name + ", ipAddress=" + ipAddress
				+ ", enableDisableAction=" + enableDisableAction
				+ ", redistributionFlagged=" + redistributionFlagged
				+ ", arpDisable=" + arpDisable + ", statsDataAction="
				+ statsDataAction + ", extendedStats=" + extendedStats + "]";
	}

	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}
}
