package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.lib.easyui.annotations.ReportField;
import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_service_group_stats")
public class ServiceGroupStats implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;

	@Persistent
	String device;

	@Persistent
	String name;
		
	@Persistent
	@SerializedName("server_selection_fail_drop")
	Integer serverSelectionFailDrop;
	
	@SerializedName("server_selection_fail_reset")
	Integer serverSelectionFailReset;
	
	@SerializedName("service_peak_conn")
	Integer servicePeakConn;
 

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getServerSelectionFailDrop() {
		return serverSelectionFailDrop;
	}

	public void setServerSelectionFailDrop(Integer serverSelectionFailDrop) {
		this.serverSelectionFailDrop = serverSelectionFailDrop;
	}

	public Integer getServerSelectionFailReset() {
		return serverSelectionFailReset;
	}

	public void setServerSelectionFailReset(Integer serverSelectionFailReset) {
		this.serverSelectionFailReset = serverSelectionFailReset;
	}

	public Integer getServicePeakConn() {
		return servicePeakConn;
	}

	public void setServicePeakConn(Integer servicePeakConn) {
		this.servicePeakConn = servicePeakConn;
	}
	
	@Override
	public String toString() {
		return "ServiceGroupStats [accountName=" + accountName + ", device="
				+ device + ", name=" + name + ", serverSelectionFailDrop="
				+ serverSelectionFailDrop + ", serverSelectionFailReset="
				+ serverSelectionFailReset + ", servicePeakConn="
				+ servicePeakConn + "]";
	}

	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}

}
