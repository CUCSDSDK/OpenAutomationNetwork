package com.cloupia.feature.network.accounts.inventory.model;

public class AuthResponse {

	String signature;
	String description;

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "AuthResponse [signature=" + signature + ", description=" + description + "]";
	}

}
