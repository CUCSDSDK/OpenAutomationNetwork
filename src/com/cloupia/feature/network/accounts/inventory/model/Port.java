package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.lib.easyui.annotations.ReportField;
import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_server_port_detail")
public class Port implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;
	
	@ReportField(label = "Device")
	@Persistent
	String device;

	@Persistent
	String serverName;
	
	@Persistent
	@SerializedName("port-number")
	Integer portNumber;
	
	@Persistent
	String protocol;
	
	@Persistent
	Integer range;
	
	@Persistent
	@SerializedName("template-port")
	String templatePort;
	
	@Persistent
	String action;
	
	@Persistent
	@SerializedName("no-ssl")
	Integer noSsl;
	
	@Persistent
	@SerializedName("health-check-disable")
	Integer healthCheckDisable;
	
	@Persistent
	Integer weight;
	@SerializedName("conn-limit")
	
	@Persistent
	Integer connLimit;
	
	@Persistent
	@SerializedName("no-logging")
	Integer noLogging;
	
	@Persistent
	@SerializedName("stats-data-action")
	String statsDataAction;
	
	@Persistent
	@SerializedName("extended-stats")
	Integer extendedStats;

	public String getDevice() {
		return device;
	}

	public void setDevice(String host) {
		this.device = host;
	}
	
	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public Integer getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(Integer portNumber) {
		this.portNumber = portNumber;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public Integer getRange() {
		return range;
	}

	public void setRange(Integer range) {
		this.range = range;
	}

	public String getTemplatePort() {
		return templatePort;
	}

	public void setTemplatePort(String templatePort) {
		this.templatePort = templatePort;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Integer getNoSsl() {
		return noSsl;
	}

	public void setNoSsl(Integer noSsl) {
		this.noSsl = noSsl;
	}

	public Integer getHealthCheckDisable() {
		return healthCheckDisable;
	}

	public void setHealthCheckDisable(Integer healthCheckDisable) {
		this.healthCheckDisable = healthCheckDisable;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getConnLimit() {
		return connLimit;
	}

	public void setConnLimit(Integer connLimit) {
		this.connLimit = connLimit;
	}

	public Integer getNoLogging() {
		return noLogging;
	}

	public void setNoLogging(Integer noLogging) {
		this.noLogging = noLogging;
	}

	public String getStatsDataAction() {
		return statsDataAction;
	}

	public void setStatsDataAction(String statsDataAction) {
		this.statsDataAction = statsDataAction;
	}

	public Integer getExtendedStats() {
		return extendedStats;
	}

	public void setExtendedStats(Integer extendedStats) {
		this.extendedStats = extendedStats;
	}

	@Override
	public String toString() {
		return "Port [accountName=" + accountName + ", device=" + device
				+ ", serverName=" + serverName + ", portNumber=" + portNumber
				+ ", protocol=" + protocol + ", range=" + range
				+ ", templatePort=" + templatePort + ", action=" + action
				+ ", noSsl=" + noSsl + ", healthCheckDisable="
				+ healthCheckDisable + ", weight=" + weight + ", connLimit="
				+ connLimit + ", noLogging=" + noLogging + ", statsDataAction="
				+ statsDataAction + ", extendedStats=" + extendedStats + "]";
	}

	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}
	
}
