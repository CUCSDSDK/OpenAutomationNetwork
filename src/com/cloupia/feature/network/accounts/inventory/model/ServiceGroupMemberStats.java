package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_service_group_member_stats")
public class ServiceGroupMemberStats implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;

	@Persistent
	String device;

	@Persistent
	String serviceGroupName;

	@Persistent
	String name;

	@Persistent
	Integer port;

	@Persistent
	@SerializedName("curr_conn")
	Integer currConn;

	@Persistent
	@SerializedName("total_fwd_bytes")
	Integer totalFwdBytes;

	@Persistent
	@SerializedName("total_fwd_pkts")
	Integer totalFwdPkts;

	@Persistent
	@SerializedName("total_rev_bytes")
	Integer totalRevBytes;

	@Persistent
	@SerializedName("total_rev_pkts")
	Integer totalRevPkts;

	@Persistent
	@SerializedName("total_conn")
	Integer totalConn;

	@Persistent
	@SerializedName("total_rev_pkts_inspected")
	Integer totalRevPktsInspected;

	@Persistent
	@SerializedName("total_rev_pkts_inspected_status_code_2xx")
	Integer totalRevPktsInspectedStatusCode2xx;

	@Persistent
	@SerializedName("total_rev_pkts_inspected_status_code_non_5xx")
	Integer totalRevPktsInspectedStatusCodeNon5xx;

	@Persistent
	@SerializedName("curr_req")
	Integer currReq;

	@Persistent
	@SerializedName("total_req")
	Integer totalReq;

	@Persistent
	@SerializedName("total_req_succ")
	Integer totalReqSucc;

	@Persistent
	@SerializedName("peak_conn")
	Integer peakConn;

	@Persistent
	@SerializedName("response_time")
	Integer reponseTime;

	@Persistent
	@SerializedName("fastest_rsp_time")
	Integer fastestRspTime;

	@Persistent
	@SerializedName("slowest_rsp_time")
	Integer slowestRspTime;

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getServiceGroupName() {
		return serviceGroupName;
	}

	public void setServiceGroupName(String serviceGroupName) {
		this.serviceGroupName = serviceGroupName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getCurrConn() {
		return currConn;
	}

	public void setCurrConn(Integer currConn) {
		this.currConn = currConn;
	}

	public Integer getTotalFwdBytes() {
		return totalFwdBytes;
	}

	public void setTotalFwdBytes(Integer totalFwdBytes) {
		this.totalFwdBytes = totalFwdBytes;
	}

	public Integer getTotalFwdPkts() {
		return totalFwdPkts;
	}

	public void setTotalFwdPkts(Integer totalFwdPkts) {
		this.totalFwdPkts = totalFwdPkts;
	}

	public Integer getTotalRevBytes() {
		return totalRevBytes;
	}

	public void setTotalRevBytes(Integer totalRevBytes) {
		this.totalRevBytes = totalRevBytes;
	}

	public Integer getTotalRevPkts() {
		return totalRevPkts;
	}

	public void setTotalRevPkts(Integer totalRevPkts) {
		this.totalRevPkts = totalRevPkts;
	}

	public Integer getTotalConn() {
		return totalConn;
	}

	public void setTotalConn(Integer totalConn) {
		this.totalConn = totalConn;
	}

	public Integer getTotalRevPktsInspected() {
		return totalRevPktsInspected;
	}

	public void setTotalRevPktsInspected(Integer totalRevPktsInspected) {
		this.totalRevPktsInspected = totalRevPktsInspected;
	}

	public Integer getTotalRevPktsInspectedStatusCode2xx() {
		return totalRevPktsInspectedStatusCode2xx;
	}

	public void setTotalRevPktsInspectedStatusCode2xx(
			Integer totalRevPktsInspectedStatusCode2xx) {
		this.totalRevPktsInspectedStatusCode2xx = totalRevPktsInspectedStatusCode2xx;
	}

	public Integer getTotalRevPktsInspectedStatusCodeNon5xx() {
		return totalRevPktsInspectedStatusCodeNon5xx;
	}

	public void setTotalRevPktsInspectedStatusCodeNon5xx(
			Integer totalRevPktsInspectedStatusCodeNon5xx) {
		this.totalRevPktsInspectedStatusCodeNon5xx = totalRevPktsInspectedStatusCodeNon5xx;
	}

	public Integer getCurrReq() {
		return currReq;
	}

	public void setCurrReq(Integer currReq) {
		this.currReq = currReq;
	}

	public Integer getTotalReq() {
		return totalReq;
	}

	public void setTotalReq(Integer totalReq) {
		this.totalReq = totalReq;
	}

	public Integer getTotalReqSucc() {
		return totalReqSucc;
	}

	public void setTotalReqSucc(Integer totalReqSucc) {
		this.totalReqSucc = totalReqSucc;
	}

	public Integer getPeakConn() {
		return peakConn;
	}

	public void setPeakConn(Integer peakConn) {
		this.peakConn = peakConn;
	}

	public Integer getReponseTime() {
		return reponseTime;
	}

	public void setReponseTime(Integer reponseTime) {
		this.reponseTime = reponseTime;
	}

	public Integer getFastestRspTime() {
		return fastestRspTime;
	}

	public void setFastestRspTime(Integer fastestRspTime) {
		this.fastestRspTime = fastestRspTime;
	}

	public Integer getSlowestRspTime() {
		return slowestRspTime;
	}

	public void setSlowestRspTime(Integer slowestRspTime) {
		this.slowestRspTime = slowestRspTime;
	}

	@Override
	public String toString() {
		return "ServiceGroupMemberStats [accountName=" + accountName
				+ ", device=" + device + ", serviceGroupName="
				+ serviceGroupName + ", name=" + name + ", port=" + port
				+ ", currConn=" + currConn + ", totalFwdBytes=" + totalFwdBytes
				+ ", totalFwdPkts=" + totalFwdPkts + ", totalRevBytes="
				+ totalRevBytes + ", totalRevPkts=" + totalRevPkts
				+ ", totalConn=" + totalConn + ", totalRevPktsInspected="
				+ totalRevPktsInspected
				+ ", totalRevPktsInspectedStatusCode2xx="
				+ totalRevPktsInspectedStatusCode2xx
				+ ", totalRevPktsInspectedStatusCodeNon5xx="
				+ totalRevPktsInspectedStatusCodeNon5xx + ", currReq="
				+ currReq + ", totalReq=" + totalReq + ", totalReqSucc="
				+ totalReqSucc + ", peakConn=" + peakConn + ", reponseTime="
				+ reponseTime + ", fastestRspTime=" + fastestRspTime
				+ ", slowestRspTime=" + slowestRspTime + "]";
	}

	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}
}
