package com.cloupia.feature.network.accounts.inventory.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.model.cIM.InventoryDBItemIf;
import com.cloupia.service.cIM.inframgr.reports.simplified.ReportableIf;
import com.google.gson.annotations.SerializedName;

@PersistenceCapable(detachable = "true", table = "A10_slb_virtual_server_port_detail")
public class VirtualServerPortStats implements InventoryDBItemIf, ReportableIf {

	@Persistent
	private String accountName;
	
	@Persistent
	String device;
	
	@Persistent
	String virtualServerName;
	
	@Persistent
	@SerializedName("port-number") 
	Integer portNumber;
	
	@Persistent
	String protocol;
	
	@Persistent
	@SerializedName("curr_conn")
	Integer currConn;
	
	@Persistent
	@SerializedName("total_l4_conn")
	Integer totalL4Conn;
	
	@Persistent
	@SerializedName("total_l7_conn")
	Integer totalL7Conn;
	
	@Persistent
	@SerializedName("total_tcp_conn")
	Integer totalTcpConn;
	
	@Persistent
	@SerializedName("total_conn")
	Integer totalConn;
	
	@Persistent
	@SerializedName("total_fwd_bytes")
	Integer totalFwdBytes;
	
	@Persistent
	@SerializedName("total_fwd_pkts")
	Integer totalFwdPkts;
	
	@Persistent
	@SerializedName("total_rev_bytes")
	Integer totalRevBytes;
	
	@Persistent
	@SerializedName("total_rev_pkts")
	Integer totalRevPkts;
	
	@Persistent
	@SerializedName("total_dns_pkts")
	Integer totalDnsPkts;
	
	@Persistent
	@SerializedName("total_mf_dns_pkts")
	Integer totalMfDnsPkts;
	
	@Persistent
	@SerializedName("es_total_failure_actions")
	Integer esTotalFailureActions;
	
	@Persistent
	@SerializedName("compression_bytes_before")
	Integer compressionBytesBefore;
	
	@Persistent
	@SerializedName("compression_bytes_after")
	Integer compressionBytesAfter;
	
	@Persistent
	@SerializedName("compression_hit")
	Integer compressionHit;
	
	@Persistent
	@SerializedName("compression_miss")
	Integer compressionMiss;
	
	@Persistent
	@SerializedName("compression_miss_no_client")
	Integer compressionMissNoClient;
	
	@Persistent
	@SerializedName("compression_miss_template_exclusion")
	Integer compressMissTemplateExclusion;
	
	@Persistent
	@SerializedName("curr_req")
	Integer currReq;
	
	@Persistent
	@SerializedName("total_req")
	Integer totalReq;
	
	@Persistent
	@SerializedName("total_req_succ")
	Integer totalReqSucc;
	
	@Persistent
	@SerializedName("peak_conn")
	Integer peakConn;
	
	@Persistent
	@SerializedName("curr_conn_rate")
	Integer currConnRate;
	
	@Persistent
	@SerializedName("last_rsp_time")
	Integer lastRspTime;
	
	@Persistent
	@SerializedName("fastest_rsp_time")
	Integer fastestRspTime;
	
	@Persistent
	@SerializedName("slowest_rsp_time")
	Integer slowestRspTime;
	

	

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}
	
	public String getVirtualServerName() {
		return virtualServerName;
	}

	public void setVirtualServerName(String virtualServerName) {
		this.virtualServerName = virtualServerName;
	}
	
	public Integer getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(Integer portNumber) {
		this.portNumber = portNumber;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public Integer getCurrConn() {
		return currConn;
	}

	public void setCurrConn(Integer currConn) {
		this.currConn = currConn;
	}

	public Integer getTotalL4Conn() {
		return totalL4Conn;
	}

	public void setTotalL4Conn(Integer totalL4Conn) {
		this.totalL4Conn = totalL4Conn;
	}

	public Integer getTotalL7Conn() {
		return totalL7Conn;
	}

	public void setTotalL7Conn(Integer totalL7Conn) {
		this.totalL7Conn = totalL7Conn;
	}

	public Integer getTotalTcpConn() {
		return totalTcpConn;
	}

	public void setTotalTcpConn(Integer totalTcpConn) {
		this.totalTcpConn = totalTcpConn;
	}

	public Integer getTotalConn() {
		return totalConn;
	}

	public void setTotalConn(Integer totalConn) {
		this.totalConn = totalConn;
	}

	public Integer getTotalFwdBytes() {
		return totalFwdBytes;
	}

	public void setTotalFwdBytes(Integer totalFwdBytes) {
		this.totalFwdBytes = totalFwdBytes;
	}

	public Integer getTotalFwdPkts() {
		return totalFwdPkts;
	}

	public void setTotalFwdPkts(Integer totalFwdPkts) {
		this.totalFwdPkts = totalFwdPkts;
	}

	public Integer getTotalRevBytes() {
		return totalRevBytes;
	}

	public void setTotalRevBytes(Integer totalRevBytes) {
		this.totalRevBytes = totalRevBytes;
	}

	public Integer getTotalRevPkts() {
		return totalRevPkts;
	}

	public void setTotalRevPkts(Integer totalRevPkts) {
		this.totalRevPkts = totalRevPkts;
	}

	public Integer getTotalDnsPkts() {
		return totalDnsPkts;
	}

	public void setTotalDnsPkts(Integer totalDnsPkts) {
		this.totalDnsPkts = totalDnsPkts;
	}

	public Integer getTotalMfDnsPkts() {
		return totalMfDnsPkts;
	}

	public void setTotalMfDnsPkts(Integer totalMfDnsPkts) {
		this.totalMfDnsPkts = totalMfDnsPkts;
	}

	public Integer getEsTotalFailureActions() {
		return esTotalFailureActions;
	}

	public void setEsTotalFailureActions(Integer esTotalFailureActions) {
		this.esTotalFailureActions = esTotalFailureActions;
	}

	public Integer getCompressionBytesBefore() {
		return compressionBytesBefore;
	}

	public void setCompressionBytesBefore(Integer compressionBytesBefore) {
		this.compressionBytesBefore = compressionBytesBefore;
	}

	public Integer getCompressionBytesAfter() {
		return compressionBytesAfter;
	}

	public void setCompressionBytesAfter(Integer compressionBytesAfter) {
		this.compressionBytesAfter = compressionBytesAfter;
	}

	public Integer getCompressionHit() {
		return compressionHit;
	}

	public void setCompressionHit(Integer compressionHit) {
		this.compressionHit = compressionHit;
	}

	public Integer getCompressionMiss() {
		return compressionMiss;
	}

	public void setCompressionMiss(Integer compressionMiss) {
		this.compressionMiss = compressionMiss;
	}

	public Integer getCompressionMissNoClient() {
		return compressionMissNoClient;
	}

	public void setCompressionMissNoClient(Integer compressionMissNoClient) {
		this.compressionMissNoClient = compressionMissNoClient;
	}

	public Integer getCompressMissTemplateExclusion() {
		return compressMissTemplateExclusion;
	}

	public void setCompressMissTemplateExclusion(
			Integer compressMissTemplateExclusion) {
		this.compressMissTemplateExclusion = compressMissTemplateExclusion;
	}

	public Integer getCurrReq() {
		return currReq;
	}

	public void setCurrReq(Integer currReq) {
		this.currReq = currReq;
	}

	public Integer getTotalReq() {
		return totalReq;
	}

	public void setTotalReq(Integer totalReq) {
		this.totalReq = totalReq;
	}

	public Integer getTotalReqSucc() {
		return totalReqSucc;
	}

	public void setTotalReqSucc(Integer totalReqSucc) {
		this.totalReqSucc = totalReqSucc;
	}

	public Integer getPeakConn() {
		return peakConn;
	}

	public void setPeakConn(Integer peakConn) {
		this.peakConn = peakConn;
	}

	public Integer getCurrConnRate() {
		return currConnRate;
	}

	public void setCurrConnRate(Integer currConnRate) {
		this.currConnRate = currConnRate;
	}

	public Integer getLastRspTime() {
		return lastRspTime;
	}

	public void setLastRspTime(Integer lastRspTime) {
		this.lastRspTime = lastRspTime;
	}

	public Integer getFastestRspTime() {
		return fastestRspTime;
	}

	

	public void setFastestRspTime(Integer fastestRspTime) {
		this.fastestRspTime = fastestRspTime;
	}

	public Integer getSlowestRspTime() {
		return slowestRspTime;
	}

	public void setSlowestRspTime(Integer slowestRspTime) {
		this.slowestRspTime = slowestRspTime;
	}
	
	@Override
	public String toString() {
		return "VirtualServerPortStats [accountName=" + accountName
				+ ", device=" + device + ", virtualServerName="
				+ virtualServerName + ", portNumber=" + portNumber
				+ ", protocol=" + protocol + ", currConn=" + currConn
				+ ", totalL4Conn=" + totalL4Conn + ", totalL7Conn="
				+ totalL7Conn + ", totalTcpConn=" + totalTcpConn
				+ ", totalConn=" + totalConn + ", totalFwdBytes="
				+ totalFwdBytes + ", totalFwdPkts=" + totalFwdPkts
				+ ", totalRevBytes=" + totalRevBytes + ", totalRevPkts="
				+ totalRevPkts + ", totalDnsPkts=" + totalDnsPkts
				+ ", totalMfDnsPkts=" + totalMfDnsPkts
				+ ", esTotalFailureActions=" + esTotalFailureActions
				+ ", compressionBytesBefore=" + compressionBytesBefore
				+ ", compressionBytesAfter=" + compressionBytesAfter
				+ ", compressionHit=" + compressionHit + ", compressionMiss="
				+ compressionMiss + ", compressionMissNoClient="
				+ compressionMissNoClient + ", compressMissTemplateExclusion="
				+ compressMissTemplateExclusion + ", currReq=" + currReq
				+ ", totalReq=" + totalReq + ", totalReqSucc=" + totalReqSucc
				+ ", peakConn=" + peakConn + ", currConnRate=" + currConnRate
				+ ", lastRspTime=" + lastRspTime + ", fastestRspTime="
				+ fastestRspTime + ", slowestRspTime=" + slowestRspTime + "]";
	}
	
	@Override
	public String getInstanceQuery() {
		// TODO Auto-generated method stub
		return "accountName == '" + this.accountName + "'";
	}

	@Override
	public String getAccountName() {
		return accountName;
	}

	@Override
	public void setAccountName(String accName) {
		accountName = accName;
	}
	
}
