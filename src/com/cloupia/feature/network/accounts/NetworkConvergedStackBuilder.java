package com.cloupia.feature.network.accounts;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.constants.ModuleConstants;
import com.cloupia.lib.cIaaS.network.model.DeviceStatus;
import com.cloupia.model.cIM.ConvergedStackComponentDetail;
import com.cloupia.model.cIM.ReportContextRegistry;
import com.cloupia.service.cIM.inframgr.reports.contextresolve.ConvergedStackComponentBuilderIf;

public class NetworkConvergedStackBuilder implements ConvergedStackComponentBuilderIf{
	Logger logger =Logger.getLogger(NetworkConvergedStackBuilder.class);

	@Override
	public ConvergedStackComponentDetail buildConvergedStackComponent(
			String accountName) throws Exception {
		logger.info("inside buildConvergedStack method start ");
		ConvergedStackComponentDetail detail = new ConvergedStackComponentDetail();
		detail.setModel("Thunder");
		detail.setOsVersion("4.0");
		detail.setVendorLogoUrl("/app/uploads/openauto/cisco.png");
		detail.setIconUrl("/app/uploads/openauto/cisco.png");
		detail.setMgmtIPAddr("172.31.240.213");
		detail.setStatus("OK");
		detail.setLabel("Thunder Lable");
		detail.setVendorName("A10");
		detail.setContextValue(accountName);
		detail.setContextType(ReportContextRegistry.getInstance().getContextByName(ModuleConstants.INFRA_ACCOUNT_TYPE).getType());
		detail.setLayerType(2);
		detail.setId("A10@172.31.240.213");
		detail.setComponentSummaryList(null);
		logger.info("inside buildConvergedStack method End ");
		
		return detail;
	}

	private List<String> getSummaryReports()
			throws Exception {
		logger.info("inside getSummaryReport method ");
		
		 List<String> rpSummaryList = new ArrayList<String>();
		 rpSummaryList.add("test");
		 rpSummaryList.add("test2");
				return rpSummaryList;
	
	}
}
