package com.cloupia.feature.network.accounts.handler;

import org.apache.log4j.Logger;

import com.cisco.cuic.api.client.JSON;
import com.cloupia.feature.network.accounts.NetworkAccount;
import com.cloupia.feature.network.accounts.reports.NetworkSampleReportData;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.lib.cIaaS.network.model.DeviceCredential;
import com.cloupia.lib.cIaaS.network.model.DeviceStatus;
import com.cloupia.lib.connector.account.AbstractInfraAccount;
import com.cloupia.lib.connector.account.AccountUtil;
import com.cloupia.lib.connector.account.PhysicalConnectivityStatus;
import com.cloupia.lib.connector.account.PhysicalConnectivityTestHandler;
import com.cloupia.lib.connector.account.PhysicalInfraAccount;

/**
 * This class is used to test the connection of the real device reachable from
 * the UCSD.
 * 
 */
public class NetworkTestConnectionHandler extends PhysicalConnectivityTestHandler {
	static Logger logger = Logger.getLogger(NetworkTestConnectionHandler.class);

	@Override
	public PhysicalConnectivityStatus testConnection(String accountName) throws Exception {

		NetworkAccount acc = getA10Credential(accountName);
		
		DeviceCredential deviceCredential = getNetworkDeviceCredential(accountName);
		
		PhysicalInfraAccount infraAccount = AccountUtil.getAccountByName(accountName);
		PhysicalConnectivityStatus status = new PhysicalConnectivityStatus(infraAccount);
		
		/**
		 * Hard coding the device details for showing in UI for Demo purpose
		 * Device details required because  need to show the device details in GUI
		 * 
		 */
		// status.setVendor("A10");
		status.setModel("Thunder 3530S HVA ");
		status.setSwVersion("TH35A83113130002");
		status.setVendor("TH35A83113130002");
		status.setSerialNumber("Serv 0.10.7");
		status.setOsType("1.0.0.26");
		status.setDeviceName("Thunder");
		status.setConnectionOK(true);

		DeviceStatus deviceStatus = new DeviceStatus();
		logger.info("inside Handlerclass");
		deviceStatus.setDeivceName("Thunder");
		deviceStatus.setDeviceIp(acc.getDeviceIp());
		deviceStatus.setDatacenter("SDK");
		deviceStatus.setDeviceOSVersion("Sw verions TH35A83113130002");
		deviceStatus.setDeviceSerialNumber("Serv 0.10.7");
		deviceStatus.setDeviceOSType("TH35A83113130002");
		deviceStatus.setDeviceModel("Thunder 3530S HVA ");
		deviceStatus.setDeviceVendor("A10 networks");
		deviceStatus.setReachable(true);
		deviceStatus.setLastCollection(System.currentTimeMillis());
		ObjStore<DeviceStatus> store = ObjStoreHelper.getStore(DeviceStatus.class);
		store.insert(deviceStatus);
		logger.info("After insert the deviceStatus");

		
		/**
		 * Hard coding the ReportData details for showing in UI for Demo purpose
		 * Here we are persisting the reportdata and showing in UI 
		 * 
		 */
		//////////////////////////////////////////////////////////////////////////////////////
		ObjStore<NetworkSampleReportData> a10SampleReportData = ObjStoreHelper
				.getStore(NetworkSampleReportData.class);
		NetworkSampleReportData a10=new NetworkSampleReportData();
		logger.info("start inserting the reportdata");
		a10.setvCPUs("2");
		a10.setDisk("16");
		a10.setRam("12GB");
		a10.setvCPUHours("251.59");
		a10.setDiskGBHours("2102.71");
		a10SampleReportData.insert(a10);
		logger.info("End inserting the reportdata");
		
		
		/////////////////////////////////////////////////////////////////////////////////////
		
		/*
		 * if (infraAccount != null) { if (infraAccount.getAccountType() !=
		 * null) { logger.info(infraAccount.getAccountType()); if
		 * (infraAccount.getAccountType().equals(ModuleConstants.
		 * INFRA_ACCOUNT_TYPE)) {
		 * 
		 * status.setConnectionOK(true); logger.debug("Connection is verified");
		 * 
		 * } }
		 * 
		 * }
		 */
		logger.info("Returning status " + status.isConnectionOK());
		return status;
	}

	private static NetworkAccount getA10Credential(String accountName) throws Exception {
		PhysicalInfraAccount acc = AccountUtil.getAccountByName(accountName);
		String json = acc.getCredential();
		AbstractInfraAccount specificAcc = (AbstractInfraAccount) JSON.jsonToJavaObject(json, NetworkAccount.class);
		specificAcc.setAccount(acc);
		return (NetworkAccount) specificAcc;

	}

	private static DeviceCredential getNetworkDeviceCredential(String accountName) throws Exception {
		PhysicalInfraAccount acc = AccountUtil.getAccountByName(accountName);
		DeviceCredential dc = acc.toDeviceCredential();
		
		return (DeviceCredential) dc;

	}
}
