package com.cloupia.feature.network.accounts.api;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.NetworkAccount;

public class NetworkAccountAPI {

	private static final HashMap<String, NetworkAccountAPI> instances = new HashMap<String, NetworkAccountAPI>();
	private static Logger logger = Logger.getLogger(NetworkAccountAPI.class);
	private String ipAddress;
	private String username;
	private String password;
	private String protocol;
	private int port;
	private int apiVersion = 1;

	private String token = null;

	/**
	 * THIS CONSTRUCTOR SHOULD NEVER BE USED, IT'S PRIVATE TO PREVENT ANYONE
	 * FROM ACCESS TO IT!
	 */
	private NetworkAccountAPI() {
	}

	/**
	 * private constructor with parameters.
	 */

	private NetworkAccountAPI(String ipAddress, String username, String password,
			int port, String protocol) {
		this.ipAddress = ipAddress;
		this.username = username;
		this.password = password;
		this.port = port;
		this.protocol = protocol;
	}

	/**
	 * This method is used to get FooAccountAPI object
	 * 
	 * @param FooAccount
	 *            having the account details.
	 * @return FooAccountAPI.
	 */
	public static NetworkAccountAPI getA10AccountAPI(NetworkAccount account)
			throws Exception {
		return getInstanceFor(account.getServerAddress(), account.getLogin(),
				account.getPassword(), Integer.parseInt(account.getPort()),
				account.getProtocol());
	}

	/**
	 * This method is used to get FooAccountAPI object
	 * 
	 * @param ipAddress
	 *            of account.
	 * @param username
	 *            of account.
	 * @param password
	 *            of account.
	 * @param protocol
	 *            of account.
	 * @return FooAccountAPI.
	 */
	public static NetworkAccountAPI getInstanceFor(String ipAddress,
			String username, String password, int port, String protocol)
			throws Exception {
		NetworkAccountAPI api = instances.get(ipAddress + username + password
				+ port);
		if (api == null) {
			api = new NetworkAccountAPI(ipAddress, username, password, port,
					protocol);

			instances.put(ipAddress + username + password + port, api);
		} else {

		}
		return api;
	}

	public String getInventoryData(String url) throws Exception {
		return url;

	}

}
