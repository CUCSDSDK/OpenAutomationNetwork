package com.cloupia.feature.network.accounts.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.inventory.model.Ethernet;
import com.cloupia.service.cIM.inframgr.collector.model.ItemResponse;


public class NetworkAccountJSONBinder extends AbstractNetworkJSONBinder {
	private static Logger logger = Logger.getLogger(NetworkAccountJSONBinder.class);
	
	@Override
	public ItemResponse bind(ItemResponse bindable) {
		return bindable;	
	}

	@Override
	public List<Class> getPersistantClassList() {
		List<Class> cList = new ArrayList<Class>();
		// add the Persistant class in the CList , for reference.
		cList.add(Ethernet.class);
		return cList;
	}
	
}
