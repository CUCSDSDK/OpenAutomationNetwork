package com.cloupia.feature.network;


import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.NetworkAccount;
import com.cloupia.feature.network.accounts.NetworkConvergedStackBuilder;
import com.cloupia.feature.network.accounts.NetworkInventoryListener;
import com.cloupia.feature.network.accounts.handler.NetworkTestConnectionHandler;
import com.cloupia.feature.network.accounts.inventory.InterfaceEthernetItemHandler;
import com.cloupia.feature.network.accounts.inventory.ServerItemHandler;
import com.cloupia.feature.network.accounts.reports.NetworkSampleReport;
import com.cloupia.feature.network.constants.ModuleConstants;
import com.cloupia.feature.network.lovs.ServerNameProvider;
import com.cloupia.feature.network.tasks.CreateServerPortsTask;
import com.cloupia.feature.network.tasks.GetAllServersTask;
import com.cloupia.lib.connector.ConfigItemDef;
import com.cloupia.lib.connector.account.AccountTypeEntry;
import com.cloupia.lib.connector.account.PhysicalAccountTypeManager;
import com.cloupia.model.cIM.InfraAccountTypes;
import com.cloupia.model.cIM.ReportContextRegistry;
import com.cloupia.service.cIM.inframgr.AbstractCloupiaModule;
import com.cloupia.service.cIM.inframgr.AbstractTask;
import com.cloupia.service.cIM.inframgr.CustomFeatureRegistry;
import com.cloupia.service.cIM.inframgr.collector.controller.CollectorFactory;
import com.cloupia.service.cIM.inframgr.reports.simplified.CloupiaReport;

public class NetworkModule extends AbstractCloupiaModule {
	Logger logger=Logger.getLogger(NetworkModule.class);

	@Override
	public AbstractTask[] getTasks() {
		
		AbstractTask task2 = new GetAllServersTask();
		
		AbstractTask task9 = new CreateServerPortsTask();
		
		

		AbstractTask[] taskarray = new AbstractTask[2];
		taskarray[0] = task2;		
		taskarray[1] = task9;
	
		
		return taskarray;
	}

	@Override
	public CollectorFactory[] getCollectors() {
		return null;
	}

	@Override
	public CloupiaReport[] getReports() {
		logger.info(" getReport start ");
		CloupiaReport[] reports = new CloupiaReport[1];
		reports[0]=new NetworkSampleReport();
//		reports[0] = new EthernetReport();
//		reports[1] = new SlbServerReport();
//		reports[2] = new ServerPortReport();
//		reports[3] = new VirtualServerReport();
//		reports[4] = new SlbVirtualServerPortReport();
//		reports[5] = new SlbServiceGroupReport();
//		reports[6] = new SlbServiceGroupMemberReport();
//		reports[7] = new SlbServerStatsReport();
//		reports[8] = new SlbServerPortStatsReport();
//		reports[9] = new SlbVirtualServerPortStatsReport();
//		reports[10] = new SlbServiceGroupStatsReport();
//		reports[11] = new SlbServiceGroupMemberStatsReport();
		logger.info(" getReport end");

		return reports;
	}

	@Override
	public void onStart(CustomFeatureRegistry cfr) {
		/**
		try {
			ReportContextRegistry.getInstance().register(
					ModuleConstants.MENU_CONTEXT,
					ModuleConstants.MENU_CONTEXT_LABEL);

			// register the left hand menu provider for the menu item i'm
			// introducing
			MenuProvider menuProvider = new MenuProvider();
			menuProvider.registerWithProvider();

		} catch (Exception e) {
			e.printStackTrace();
		}
		**/
		
		try {
			logger.info(" Inside Onstart method start ");
			ReportContextRegistry.getInstance().register(
					ModuleConstants.INFRA_ACCOUNT_TYPE,
					ModuleConstants.INFRA_ACCOUNT_LABEL);
			
		} catch (Exception e1) {
			logger.info(" Inside Onstart method catchblock ");
			e1.printStackTrace();
		}
		
		try {
			cfr.registerLovProviders(ServerNameProvider.SERVER_NAME_LOV,
					new ServerNameProvider());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		AccountTypeEntry entry = new AccountTypeEntry();

		// This is mandatory, hold the information for device credential details
		entry.setCredentialClass(NetworkAccount.class);

		// This is mandatory, type of the Account will be shown in GUI as drill
		// down box
		entry.setAccountType(ModuleConstants.INFRA_ACCOUNT_TYPE);

		// This is mandatory, label of the Account
		entry.setAccountLabel(ModuleConstants.INFRA_ACCOUNT_LABEL);

		// This is mandatory, specify the category of the account type ie.,
		// Network / Storage / //Compute
		entry.setCategory(InfraAccountTypes.CAT_NETWORK);
		
		// This is mandatory
		entry.setContextType(ReportContextRegistry.getInstance()
				.getContextByName(ModuleConstants.INFRA_ACCOUNT_TYPE).getType());

		// This is mandatory, on which accounts either physical or virtual
		// account , new account type belong to.
		//For Network account , should be AccountTypeEntry.NETWORK_ACCOUNT
		entry.setAccountClass(AccountTypeEntry.NETWORK_ACCOUNT);
		// Optional , prefix of the task
		entry.setInventoryTaskPrefix("A10 Inventory Task");
		// Optional , collect the inventory frequency, whenever required you can
		// change the inventory collection frequency, in mins.
		entry.setInventoryFrequencyInMins(15);
		// This is mandatory,under which pod type , the new account type is
		// applicable.
		entry.setPodTypes(new String[] { "FlexPod" });
		
		//instead of loading image and vendor from device-icon-mapping xml file its  loading from bellow code
		//also it will set the context for leftside tree structure so that we are able see the menu report
		
		entry.setVendor("A10 networks");
		entry.setIconPath("/app/uploads/openauto/a10_thunder.png");
		
		// This is optional, dependents on the need of session for collecting
		// the inventory
		// entry.setConnectorSessionFactory(new FooSessionFactory());

		// This is mandatory, to test the connectivity of the new account. The
		// Handler should be of type PhysicalConnectivityTestHandler.
		entry.setTestConnectionHandler(new NetworkTestConnectionHandler());
		// This is mandatory, we can implement inventory listener according to
		// the account Type , collect the inventory details.
		
		entry.setInventoryListener(new NetworkInventoryListener());

		// This is mandatory , to show in the converged stack view
		entry.setConvergedStackComponentBuilder(new NetworkConvergedStackBuilder());

		// This is required to show up the details of the stack view in the GUI
		// entry.setStackViewItemProvider(new DummyStackViewProvider());

		// This is required credential.If the Credential Policy support is
		// required for this Account type then this is mandatory, can implement
		// credential check against the policyname.
		// entry.setCredentialParser(new FooAccountCredentialParser());
		
		try {

			// Adding inventory root
			registerInventoryObjects(entry);
			PhysicalAccountTypeManager.getInstance().addNewAccountType(entry);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * This method is used to register the inventory objects
	 * 
	 * @param entry
	 */
	
	private void registerInventoryObjects(AccountTypeEntry entry) {
		
		  ConfigItemDef A10InterfaceEthernetInfo =
		         entry.createInventoryRoot("A10.inventory.root.interface.ethernet",  InterfaceEthernetItemHandler.class);
		  
	}
}

