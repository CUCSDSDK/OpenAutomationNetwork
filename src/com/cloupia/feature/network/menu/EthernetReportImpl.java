package com.cloupia.feature.network.menu;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.inventory.model.Ethernet;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.model.cIM.ReportContext;
import com.cloupia.model.cIM.TabularReport;
import com.cloupia.service.cIM.inframgr.TabularReportGeneratorIf;
import com.cloupia.service.cIM.inframgr.reportengine.ReportRegistryEntry;
import com.cloupia.service.cIM.inframgr.reports.TabularReportInternalModel;

public class EthernetReportImpl implements TabularReportGeneratorIf {
	private static Logger logger = Logger.getLogger(EthernetReportImpl.class);
	
	@Override
	public TabularReport getTabularReportReport(
			ReportRegistryEntry reportEntry, ReportContext context)
			throws Exception {
		TabularReport report = new TabularReport();

		report.setGeneratedTime(System.currentTimeMillis());
		report.setReportName(reportEntry.getReportLabel());
		report.setContext(context);
		logger.info("EthernetReportImpl:: context.getId():" + context.getId());
		int index = context.getId().indexOf(";");
		String accountName = context.getId().substring(0,index);
		String query = "accountName == '" + accountName + "'";
		logger.info("EthernetReportImpl:: query:" + query);
		TabularReportInternalModel model = new TabularReportInternalModel();

		model.addTextColumn("Device", "Device");
		model.addTextColumn("Interface Number", "Interface Number");
		model.addTextColumn("L3 VLAN forwarding Disable",
				"L3 VLAN forwarding Disable");
		model.addTextColumn("MTU", "MTU");
		model.addTextColumn("Load Interval", "Load Interval");
		model.addTextColumn("Trap Source", "Trap Source");
		model.addTextColumn("Duplexity", "Duplexity");
		model.addTextColumn("Speed", "Speed");
		model.addTextColumn("Flow Control", "Flow Control");
		model.addTextColumn("Action", "Action");
		model.completedHeader();

		ObjStore<Ethernet> store = ObjStoreHelper.getStore(Ethernet.class);
		List<Ethernet> objs = store.query(query);
		logger.info("EthernetReportImpl::" + objs);
		for (int i=0; i<objs.size(); i++){
			Ethernet ethernet = objs.get(i);
		
			model.addTextValue("" + ethernet.getDevice());
			model.addTextValue("" + ethernet.getIfnum());
			model.addTextValue("" + ethernet.getL3VlanFwdDisable());
			model.addTextValue("" + ethernet.getMtu());
			model.addTextValue("" + ethernet.getLoadInterval());
			model.addTextValue("" + ethernet.getTrapSource());
			model.addTextValue("" + ethernet.getDuplexity());
			model.addTextValue("" + ethernet.getSpeed());
			model.addTextValue("" + ethernet.getFlowControl());
			model.addTextValue("" + ethernet.getAction());
			model.completedRow();
		}

		model.updateReport(report);
		return report;
	}
}
