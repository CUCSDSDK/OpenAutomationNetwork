package com.cloupia.feature.network.menu;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.inventory.model.Port;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.model.cIM.ReportContext;
import com.cloupia.model.cIM.TabularReport;
import com.cloupia.service.cIM.inframgr.TabularReportGeneratorIf;
import com.cloupia.service.cIM.inframgr.reportengine.ReportRegistryEntry;
import com.cloupia.service.cIM.inframgr.reports.TabularReportInternalModel;

public class ServerPortReportImpl implements TabularReportGeneratorIf {
	private static Logger logger = Logger.getLogger(ServerPortReportImpl.class);
	
	@Override
	public TabularReport getTabularReportReport(
			ReportRegistryEntry reportEntry, ReportContext context)
			throws Exception {
		TabularReport report = new TabularReport();

		report.setGeneratedTime(System.currentTimeMillis());
		report.setReportName(reportEntry.getReportLabel());
		report.setContext(context);
		logger.info("ServerPortReportImpl:: context.getId():" + context.getId());
		int index = context.getId().indexOf(";");
		String accountName = context.getId().substring(0,index);
		String query = "accountName == '" + accountName + "'";
		logger.info("ServerPortReportImpl:: query:" + query);
		TabularReportInternalModel model = new TabularReportInternalModel();

		model.addTextColumn("Device", "Device");
		model.addTextColumn("Server", "Server");
		model.addTextColumn("Port Number", "Port Number");
		model.addTextColumn("Protocol", "Protocol");
		model.addTextColumn("Range", "Range");
		model.addTextColumn("Template Port", "Template Port");
		model.addTextColumn("Action", "Action");
		model.addTextColumn("No SSL", "No SSL");
		model.addTextColumn("Health Check Disable", "Health Check Disable");
		model.addTextColumn("Weight", "Weight");
		model.addTextColumn("Connection Limit", "Connection Limit");
		model.addTextColumn("No Logging", "No Logging");
		model.addTextColumn("Stats Data Action", "Stats Data Action");
		model.addTextColumn("Extended Stats", "Extended Stats");

		model.completedHeader();

		ObjStore<Port> store = ObjStoreHelper.getStore(Port.class);
		List<Port> objs = store.query(query);
		logger.info("ServerPortReportImpl::" + objs);
		for (int i = 0; i < objs.size(); i++) {
			Port port = objs.get(i);

			model.addTextValue("" + port.getDevice());
			model.addTextValue("" + port.getServerName());
			model.addTextValue("" + port.getPortNumber());
			model.addTextValue("" + port.getProtocol());
			model.addTextValue("" + port.getRange());
			model.addTextValue("" + port.getTemplatePort());
			model.addTextValue("" + port.getAction());
			model.addTextValue("" + port.getNoSsl());
			model.addTextValue("" + port.getHealthCheckDisable());
			model.addTextValue("" + port.getWeight());
			model.addTextValue("" + port.getConnLimit());
			model.addTextValue("" + port.getNoLogging());
			model.addTextValue("" + port.getStatsDataAction());
			model.addTextValue("" + port.getExtendedStats());

			model.completedRow();
		}
		model.updateReport(report);
		return report;
	}
}
