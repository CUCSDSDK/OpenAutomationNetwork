package com.cloupia.feature.network.menu;

import com.cloupia.feature.network.constants.ModuleConstants;
import com.cloupia.model.cIM.DynReportContext;
import com.cloupia.model.cIM.ReportContextRegistry;
import com.cloupia.service.cIM.inframgr.reportengine.ContextMapRule;
import com.cloupia.service.cIM.inframgr.reports.simplified.CloupiaReportAction;
import com.cloupia.service.cIM.inframgr.reports.simplified.CloupiaReportWithActions;

public class VirtualServerReport extends CloupiaReportWithActions {
	
	private static final String NAME = "A10.slb.virtualserver.report";
	private static final String LABEL = "SLB Virtual Servers";
	
	public VirtualServerReport() {
		super();
		//IMPORTANT: this tells the framework which column of this report you want to pass as the report context id
		//when there is a UI action being launched in this report
		this.setMgmtColumnIndex(1);
	}

	@Override
	public CloupiaReportAction[] getActions() {
		return null;
	}

	@Override
	public Class getImplementationClass() {
		//this class handles all the report generation logic, look here for more details
		return VirtualServerReportImpl.class;
	}

	@Override
	public String getReportLabel() {
		return LABEL;
	}

	@Override
	public String getReportName() {
		return NAME;
	}

	//this is important, make sure this returns false.  isEasyReport should ONLY return
	//true if and only if you're using the POJO approach, which we aren't in this case.
	@Override
	public boolean isEasyReport() {
		return false;
	}

	//make sure to return true in this case, as there are no drill down reports
	@Override
	public boolean isLeafReport() {
		return true;
	}
	
	//basically hardcoding it so this report will show in the location i want it to
	@Override
	public int getMenuID() {
		// return ModuleConstants.MENU_ID;
		return 52;
	}
	
	@Override
	public ContextMapRule[] getMapRules() {
		DynReportContext menuReportContext = ReportContextRegistry.getInstance().getContextByName(ModuleConstants.INFRA_ACCOUNT_TYPE);
		// DynReportContext menuReportContext = ReportContextRegistry.getInstance().getContextByName(ModuleConstants.MENU_CONTEXT);
		ContextMapRule rule = new ContextMapRule();
		rule.setContextName(menuReportContext.getId());
		rule.setContextType(menuReportContext.getType());
		ContextMapRule[] rules = new ContextMapRule[1];
		rules[0] = rule;
		return rules;
	}
}
