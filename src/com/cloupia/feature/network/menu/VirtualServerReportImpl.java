package com.cloupia.feature.network.menu;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.inventory.model.VirtualServer;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.model.cIM.ReportContext;
import com.cloupia.model.cIM.TabularReport;
import com.cloupia.service.cIM.inframgr.TabularReportGeneratorIf;
import com.cloupia.service.cIM.inframgr.reportengine.ReportRegistryEntry;
import com.cloupia.service.cIM.inframgr.reports.TabularReportInternalModel;

public class VirtualServerReportImpl implements TabularReportGeneratorIf {

	private static Logger logger = Logger
			.getLogger(VirtualServerReportImpl.class);

	@Override
	public TabularReport getTabularReportReport(
			ReportRegistryEntry reportEntry, ReportContext context)
			throws Exception {
		TabularReport report = new TabularReport();

		report.setGeneratedTime(System.currentTimeMillis());
		report.setReportName(reportEntry.getReportLabel());
		report.setContext(context);
		int index = context.getId().indexOf(";");
		String accountName = context.getId().substring(0,index);
		String query = "accountName == '" + accountName + "'";
		
		TabularReportInternalModel model = new TabularReportInternalModel();

		model.addTextColumn("Device", "Device");
		model.addTextColumn("Name", "Name");
		model.addTextColumn("IP Address", "IP Address");
		model.addTextColumn("Enable Disable Action", "Enable Disable Action");
		model.addTextColumn("Redistribution Flagged", "Redistribution Flagged");
		model.addTextColumn("ARP Disable", "ARP Disable");
		model.addTextColumn("Stats Data Action", "Stats Data Action");
		model.addTextColumn("Extended Stats", "Extended Stats");

		model.completedHeader();

		ObjStore<VirtualServer> store = ObjStoreHelper
				.getStore(VirtualServer.class);
		List<VirtualServer> objs = store.query(query);
		logger.info("VirtualServerReportImpl::" + objs);
		for (int i = 0; i < objs.size(); i++) {
			VirtualServer virtualServer = objs.get(i);

			model.addTextValue("" + virtualServer.getDevice());
			model.addTextValue("" + virtualServer.getName());
			model.addTextValue("" + virtualServer.getIpAddress());
			model.addTextValue("" + virtualServer.getEnableDisableAction());
			model.addTextValue("" + virtualServer.getRedistributionFlagged());
			model.addTextValue("" + virtualServer.getArpDisable());
		    model.addTextValue("" + virtualServer.getStatsDataAction());
			model.addTextValue("" + virtualServer.getExtendedStats());

			model.completedRow();
		}

		model.updateReport(report);
		return report;
	}
}
