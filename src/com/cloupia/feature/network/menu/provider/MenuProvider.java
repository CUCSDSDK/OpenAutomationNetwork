package com.cloupia.feature.network.menu.provider;

import java.util.ArrayList;
import java.util.List;

import com.cloupia.feature.network.constants.ModuleConstants;
import com.cloupia.model.cIM.AbstractTreeNodesProviderIf;
import com.cloupia.model.cIM.DynReportContext;
import com.cloupia.model.cIM.ReportContextRegistry;
import com.cloupia.model.cIM.UIMenuLHTreeNode;
import com.cloupia.model.cIM.UIMenuLHTreeProvider;

public class MenuProvider extends AbstractTreeNodesProviderIf {

	@Override
	public List<UIMenuLHTreeNode> getLHTreeNodes() throws Exception {
		ArrayList<UIMenuLHTreeNode> nodes = new ArrayList<UIMenuLHTreeNode>();
		DynReportContext dynReportContext = ReportContextRegistry.getInstance()
				.getContextByName(ModuleConstants.MENU_CONTEXT);
		UIMenuLHTreeNode nodeOne = new UIMenuLHTreeNode("Thunder", null,
				dynReportContext.getType(), dynReportContext.getId());
		nodes.add(nodeOne);
		return nodes;
	}

	@Override
	public void registerWithProvider() {
		UIMenuLHTreeProvider.getInstance().registerLHTreeNodeProvider( ModuleConstants.MENU_ID , this );
	}

}
