package com.cloupia.feature.network.constants;

public class ModuleConstants {
	
	public static final String INFRA_ACCOUNT_TYPE = "A10";
	public static final String INFRA_ACCOUNT_LABEL = "A10";
	
	public static final int MENU_ID = 80003;
	public static final String MENU_CONTEXT = "A10.menu.context";
	public static final String MENU_CONTEXT_LABEL = "Menu Context";
		
}

