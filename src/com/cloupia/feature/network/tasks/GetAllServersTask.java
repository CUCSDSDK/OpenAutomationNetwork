package com.cloupia.feature.network.tasks;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.connector.GetAllServers;
import com.cloupia.feature.network.accounts.inventory.model.Server;
import com.cloupia.service.cIM.inframgr.AbstractTask;
import com.cloupia.service.cIM.inframgr.TaskConfigIf;
import com.cloupia.service.cIM.inframgr.TaskOutputDefinition;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionLogger;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionTriggerContext;

public class GetAllServersTask extends AbstractTask {

	static Logger logger = Logger.getLogger(GetAllServersTask.class);

	@Override
	public void executeCustomAction(CustomActionTriggerContext context,
			CustomActionLogger actionLogger) throws Exception {

		long configEntryId = 0;
		GetAllServersTaskConfig config = null;
		try {
			configEntryId = context.getConfigEntry().getConfigEntryId();
			config = (GetAllServersTaskConfig) context.loadConfigObject();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (config == null) {
			throw new Exception("No configuration found for custom action "
					+ context.getActionDef().getName() + " entryId "
					+ configEntryId);
		}

		actionLogger
				.addInfo("----- Before REST Call to get all slb Servers -----");

		List<Server> sl = GetAllServers.getAllSlbServers(
				config.getTransportProtocol(), config.getIpAddress(),
				config.getLogin(), config.getPassword(),
				Integer.parseInt(config.getPort()));
		if (null != sl) {
			for (Server s : sl) {
				actionLogger
						.addInfo("**************************************************************");
				actionLogger.addInfo(s.toString());
				actionLogger
						.addInfo("**************************************************************");
			}
		} else {
			actionLogger
					.addInfo("----- REST Call to get all slb servers FAILED -----");
		}
		actionLogger
				.addInfo("---- Successfully executed get all slb servers Task ------");

	}

	@Override
	public TaskConfigIf getTaskConfigImplementation() {
		return new GetAllServersTaskConfig();
	}

	@Override
	public String getTaskName() {
		return GetAllServersTaskConfig.displayLabel;
	}

	@Override
	public TaskOutputDefinition[] getTaskOutputDefinitions() {
		return null;
	}
}
