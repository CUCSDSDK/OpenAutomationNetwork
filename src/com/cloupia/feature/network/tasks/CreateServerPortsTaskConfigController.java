package com.cloupia.feature.network.tasks;

import org.apache.log4j.Logger;

import com.cloupia.model.cIM.ReportContext;
import com.cloupia.service.cIM.inframgr.forms.wizard.AbstractObjectUIController;
import com.cloupia.service.cIM.inframgr.forms.wizard.Page;


public class CreateServerPortsTaskConfigController extends AbstractObjectUIController {
	
	static Logger logger = Logger.getLogger(CreateServerPortsTaskConfigController.class);

    @Override
    public void afterBind(Page page, String id, ReportContext context)
    {
    }
    
	@Override
	public void beforeMarshall(Page page, String id, ReportContext context,
			Object pojo) throws Exception {
		CreateServerPortsTaskConfig config = (CreateServerPortsTaskConfig) pojo;
		if (config.getRange() == null)
			config.setRange("" + 0);
		if (config.getTemplatePort() == null)
			config.setTemplatePort("default");
		if (config.getAction() == null)
			config.setAction("enable");
		if (config.getNoSsl() == null)
			config.setNoSsl("" + 0);
		if (config.getHealthCheckDisable() == null)
			config.setHealthCheckDisable("" + 0);
		if (config.getWeight() == null)
			config.setWeight("" + 1);
		if (config.getConnLimit() == null)
			config.setConnLimit("" + 8000000);
		if (config.getNoLogging() == null)
			config.setNoLogging("" + 0);
		if (config.getStatsDataAction() == null)
			config.setStatsDataAction("stats-data-enable");
		if (config.getExtendedStats() == null)
			config.setExtendedStats("" + 0);
	}
    
    @Override
    public void afterUnmarshall(Page page, String id, ReportContext context, Object pojo) throws Exception
    {
        if (page.isPageSubmitted())
        {
        	
        }
    }
}