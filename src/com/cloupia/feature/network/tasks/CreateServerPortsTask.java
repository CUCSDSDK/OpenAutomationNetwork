package com.cloupia.feature.network.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.network.accounts.inventory.model.Port;
import com.cloupia.service.cIM.inframgr.AbstractTask;
import com.cloupia.service.cIM.inframgr.TaskConfigIf;
import com.cloupia.service.cIM.inframgr.TaskOutputDefinition;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionLogger;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionTriggerContext;

public class CreateServerPortsTask extends AbstractTask {

	static Logger logger = Logger.getLogger(CreateServerPortsTask.class);

	@Override
	public void executeCustomAction(CustomActionTriggerContext context,
			CustomActionLogger actionLogger) throws Exception {

		long configEntryId = 0;
		CreateServerPortsTaskConfig config = null;
		try {
			configEntryId = context.getConfigEntry().getConfigEntryId();
			config = (CreateServerPortsTaskConfig) context
					.loadConfigObject();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (config == null) {
			throw new Exception("No configuration found for custom action "
					+ context.getActionDef().getName() + " entryId "
					+ configEntryId);
		}

		actionLogger
				.addInfo("----- Before REST Call to configure slb Server Ports -----");

		List<Port> pl = new ArrayList<Port>();

		Port p = new Port();

		p.setPortNumber(Integer.parseInt(config.getPortNumber()));
		p.setProtocol(config.getProtocol());
		p.setRange(Integer.parseInt(config.getRange()));
		p.setTemplatePort(config.getTemplatePort());
		p.setAction(config.getAction());
		p.setNoSsl(Integer.parseInt(config.getNoSsl()));
		p.setHealthCheckDisable(Integer.parseInt(config.getHealthCheckDisable()));
		p.setWeight(Integer.parseInt(config.getWeight()));
		p.setConnLimit(Integer.parseInt(config.getConnLimit()));
		p.setNoLogging(Integer.parseInt(config.getNoLogging()));
		p.setStatsDataAction(config.getStatsDataAction());
		p.setExtendedStats(Integer.parseInt(config.getExtendedStats()));
		pl.add(p);

		

	}

	@Override
	public TaskConfigIf getTaskConfigImplementation() {
		return new CreateServerPortsTaskConfig();
	}

	@Override
	public String getTaskName() {
		return CreateServerPortsTaskConfig.displayLabel;
	}

	@Override
	public TaskOutputDefinition[] getTaskOutputDefinitions() {
		return null;
	}
}
