package com.cloupia.feature.network.tasks;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.service.cIM.inframgr.TaskConfigIf;
import com.cloupia.service.cIM.inframgr.customactions.UserInputField;
import com.cloupia.service.cIM.inframgr.forms.wizard.FormField;

@PersistenceCapable(detachable = "true", table = "networkmodule_create_slb_server_ports_table")
public class CreateServerPortsTaskConfig implements TaskConfigIf {

	@Persistent
	public static final String displayLabel = "A10: Create Slb Server Ports";
	@Persistent
	private long configEntryId;
	@Persistent
	private long actionId;
	
	@FormField(label = "Transport Protocol : ", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String transportProtocol;

	@FormField(label = "IP Address : ", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String ipAddress;
	
	@FormField(label = "login:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String login;
	

	@FormField(label = "password:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String password;
	
	@FormField(label = "port:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String port;
	
	@FormField(label = "server name:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	String serverName;
	
	
	@FormField(label = "Port Number:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String portNumber;
	

	@FormField(label = "Protocol:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String protocol;
	
	@FormField(label = "Range:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String range;
	
	@FormField(label = "Template Port:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String templatePort;
	

	@FormField(label = "Action:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String action;
	
	@FormField(label = "No SSL:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String noSsl;
	
	@FormField(label = "Health Check Disable:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String healthCheckDisable;
	
	@FormField(label = "Weight:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String weight;
	

	@FormField(label = "Connection Limit:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String connLimit;
	
	@FormField(label = "No Logging:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String noLogging;
	
	@FormField(label = "Stats Data Action:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String statsDataAction;
	
	@FormField(label = "Extended Stats:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String extendedStats;
	
	public String getTransportProtocol() {
		return transportProtocol;
	}

	public void setTransportProtocol(String transportProtocol) {
		this.transportProtocol = transportProtocol;
	}
		
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(String portNumber) {
		this.portNumber = portNumber;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getTemplatePort() {
		return templatePort;
	}

	public void setTemplatePort(String templatePort) {
		this.templatePort = templatePort;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getNoSsl() {
		return noSsl;
	}

	public void setNoSsl(String noSsl) {
		this.noSsl = noSsl;
	}

	public String getHealthCheckDisable() {
		return healthCheckDisable;
	}

	public void setHealthCheckDisable(String healthCheckDisable) {
		this.healthCheckDisable = healthCheckDisable;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getConnLimit() {
		return connLimit;
	}

	public void setConnLimit(String connLimit) {
		this.connLimit = connLimit;
	}

	public String getNoLogging() {
		return noLogging;
	}

	public void setNoLogging(String noLogging) {
		this.noLogging = noLogging;
	}

	public String getStatsDataAction() {
		return statsDataAction;
	}

	public void setStatsDataAction(String statsDataAction) {
		this.statsDataAction = statsDataAction;
	}
			
	public String getExtendedStats() {
		return extendedStats;
	}

	public void setExtendedStats(String extendedStats) {
		this.extendedStats = extendedStats;
	}

	@Override
	public long getActionId() {
		return actionId;
	}

	@Override
	public long getConfigEntryId() {
		return configEntryId;
	}

	@Override
	public String getDisplayLabel() {
		return displayLabel;
	}

	@Override
	public void setConfigEntryId(long configEntryId) {
		this.configEntryId = configEntryId;
	}

	@Override
	public void setActionId(long actionId) {
		this.actionId = actionId;
	}
}
