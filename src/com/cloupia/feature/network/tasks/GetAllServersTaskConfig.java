package com.cloupia.feature.network.tasks;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.cloupia.service.cIM.inframgr.TaskConfigIf;
import com.cloupia.service.cIM.inframgr.customactions.UserInputField;
import com.cloupia.service.cIM.inframgr.forms.wizard.FormField;

@PersistenceCapable(detachable = "true", table = "networkmodule_all_slb_servers_table")
public class GetAllServersTaskConfig implements TaskConfigIf {

	@Persistent
	public static final String displayLabel = "A10: Get All Slb Servers";
	@Persistent
	private long configEntryId;
	@Persistent
	private long actionId;
	
	@FormField(label = "Transport Protocol : ", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String transportProtocol;
	
	@FormField(label = "IP Address : ", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String ipAddress;
	
	@FormField(label = "login:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String login;
	

	@FormField(label = "password:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String password;
	
	@FormField(label = "port:", mandatory = true)
	@UserInputField(type = "gen_text_input")
    @Persistent
	private String port;
			
	public String getTransportProtocol() {
		return transportProtocol;
	}

	public void setTransportProtocol(String transportProtocol) {
		this.transportProtocol = transportProtocol;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	
	@Override
	public long getActionId() {
		return actionId;
	}

	@Override
	public long getConfigEntryId() {
		return configEntryId;
	}

	@Override
	public String getDisplayLabel() {
		return displayLabel;
	}

	@Override
	public void setConfigEntryId(long configEntryId) {
		this.configEntryId = configEntryId;
	}

	@Override
	public void setActionId(long actionId) {
		this.actionId = actionId;
	}
}
